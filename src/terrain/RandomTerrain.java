package terrain;

import static org.lwjgl.opengl.GL11.glCallList;
import static org.lwjgl.opengl.GL11.glGenLists;
import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.Rectangle;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import utility.ShaderLoader;



public class RandomTerrain 
{
	private float chunkSize;
	private int numOfChunks;
	private int iterations;
	private double roughness;
	private int displacement;
	private ChunkLoader chunkLoader;
	private Chunk m;
	
	public RandomTerrain(int iterations, double roughness, int displacement, int numOfChunks)
	{
		this.iterations = iterations;
		this.roughness = roughness;
		this.displacement = displacement;
		this.numOfChunks = numOfChunks;
		chunkLoader = new ChunkLoader(iterations, roughness, displacement);
		m = new Chunk();
		//m = chunkLoader.genChunk(m);
		m.setUpVBO();
	}

	public void draw(Rectangle bound)
	{
		m.drawChunk();
	}
}
