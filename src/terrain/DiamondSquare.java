package terrain;

import java.util.Random;

public class DiamondSquare 
{
	private float[][] terrain;
	private final int iterations;
	private double displacement;
	private double roughness;
	private double length;
	
	
	public DiamondSquare(int numberOfIterations, double roughness, double displacement)
	{
		this.iterations = numberOfIterations;
		this.roughness = roughness;
		this.displacement = displacement;
		length = (Math.pow(2, iterations) + 1);
		terrain = new float[(int)length][(int)length];
	}
	
	public float[][] generate()
	{
		double NoS; //number of squares in one direction
		initEdges(); //sets corners to 0
		int len = (int) length - 1; // len is just a more usable length
		int sl; //length or width of each seperate square
		int change; //change from center to edge of a square
		for(int n = 0; n < iterations; n++)//this for loop iterates each sweep
		{
			NoS = Math.pow(2, n); //number of squares is always 2^n
			sl = (int) (len / NoS); //square lenght is always length / number of squares
			change = sl / 2; //change is hald the square length
			for(int i = 0; i < NoS; i++) //this for loop iterates the y direction
			{
				int top = i*sl;	//top of the square in array
				int ymid = i*sl + sl/2; //middle of sqaure in array
				int bottom = i*sl + sl; //bottom of square in array
				//because of the way the array is laid out, bottom is always larger than the top
				
				for(int j = 0; j < NoS; j++)//this for loop iterates the x direction
				{
					int left = j*sl; //left if the square in array
					int xmid = j*sl + sl/2; //middle of square in array
					int right = j*sl + sl; //right of square in array
					
					float avg = 0; //variable to hold the average between adjacent points
					
					
					//Sets each corner to a float so we can find the average
					float A = terrain[left][top];
					float B = terrain[right][top];
					float C = terrain[left][bottom];
					float D = terrain[right][bottom];
					//set center point (avg of surronding points + random displacement)
					terrain[xmid][ymid] = (float) ((A+B+C+D)/4 + Rand(displacement));
					
					//set diamond points one at a time
					//each point must check if the point above,below, right, or left is available as to avoid a out of bounds exception
					//---------------------------------------------------------
					if(xmid+change > len)
					{
						//A = terrain[mid+mid][zero];
						B = terrain[xmid-change][top];
						C = terrain[xmid][top+change];
						D = terrain[xmid][top-change];
						avg = (B+C+D)/3;
					}
					else if(xmid-change < 0)
					{
						A = terrain[xmid+change][top];
						//B = terrain[mid-mid][zero];
						C = terrain[xmid][top+change];
						D = terrain[xmid][top-change];
						avg = (A+C+D)/3;
					}
					else if(top+change > len)
					{
						A = terrain[xmid+change][top];
						B = terrain[xmid-change][top];
						//C = terrain[mid][zero+mid];
						D = terrain[xmid][top-change];
						avg = (A+B+D)/3;
					}
					else if(top-change < 0)
					{
						A = terrain[xmid+change][top];
						B = terrain[xmid-change][top];
						C = terrain[xmid][top+change];
						//D = terrain[mid][zero-mid];
						avg = (A+B+C)/3;
					}
					else
					{
						A = terrain[xmid+change][top];
						B = terrain[xmid-change][top];
						C = terrain[xmid][top+change];
						D = terrain[xmid][top-change];
						avg = (A+B+C+D)/4;
					}
					terrain[xmid][top] = (float)(avg + Rand(displacement));
					
					//----------------------------------------------------------------------
					
					
					if(right+change > len)
					{
						//A = terrain[far+mid][mid];
						B = terrain[right-change][ymid];
						C = terrain[right][ymid+change];
						D = terrain[right][ymid-change];
						avg = (B+C+D)/3;
					}
					else if(right-change < 0)
					{
						A = terrain[right+change][ymid];
						//B = terrain[far-mid][mid];
						C = terrain[right][ymid+change];
						D = terrain[right][ymid-change];
						avg = (A+C+D)/3;
					}
					else if(ymid+change > len)
					{
						A = terrain[right+change][ymid];
						B = terrain[right-change][ymid];
						//C = terrain[far][mid+mid];
						D = terrain[right][ymid-change];
						avg = (A+B+D)/3;
					}
					else if(ymid-change < 0)
					{
						A = terrain[right+change][ymid];
						B = terrain[right-change][ymid];
						C = terrain[right][ymid+change];
						//D = terrain[far][mid-mid];
						avg = (A+B+C)/3;
					}
					else
					{
						A = terrain[right+change][ymid];
						B = terrain[right-change][ymid];
						C = terrain[right][ymid+change];
						D = terrain[right][ymid-change];
						avg = (A+B+C+D)/4;
					}
					
					terrain[right][ymid] = (float) (avg + Rand(displacement));
					
					//-----------------------------------------------------------------
					
					
					if(xmid+change > len)
					{
						//A = terrain[mid+mid][far];
						B = terrain[xmid-change][bottom];
						C = terrain[xmid][bottom+change];
						D = terrain[xmid][bottom-change];
						avg = (B+C+D)/3;
					}
					else if(xmid-change < 0)
					{
						A = terrain[xmid+change][bottom];
						//B = terrain[mid-mid][far];
						C = terrain[xmid][bottom+change];
						D = terrain[xmid][bottom-change];
						avg = (A+C+D)/3;
					}
					else if(bottom+change > len)
					{
						A = terrain[xmid+change][bottom];
						B = terrain[xmid-change][bottom];
						//C = terrain[mid][far+mid];
						D = terrain[xmid][bottom-change];
						avg = (A+B+D)/3;
					}
					else if(bottom-change < 0)
					{
						A = terrain[xmid+change][bottom];
						B = terrain[xmid-change][bottom];
						C = terrain[xmid][bottom+change];
						//D = terrain[mid][far-mid];
						avg = (A+B+C)/3;
					}
					else
					{
						A = terrain[xmid+change][bottom];
						B = terrain[xmid-change][bottom];
						C = terrain[xmid][bottom+change];
						D = terrain[xmid][bottom-change];
						avg = (A+B+C+D)/4;
					}
					terrain[xmid][bottom] = (float) (avg + Rand(displacement));
					
					//------------------------------------------------------------------------
					
					if(ymid+change > len)
					{
						//A = terrain[zero][mid+mid];
						B = terrain[left][ymid-change];
						C = terrain[left+change][ymid];
						D = terrain[left-change][ymid];
						avg = (B+C+D)/3;
					}
					else if(ymid-change < 0)
					{
						A = terrain[left][ymid+change];
						//B = terrain[zero][mid-mid];
						C = terrain[left+change][ymid];
						D = terrain[left-change][ymid];
						avg = (A+C+D)/3;
					}
					else if(left+change > len)
					{
						A = terrain[left][ymid+change];
						B = terrain[left][ymid-change];
						//C = terrain[zero+mid][mid];
						D = terrain[left-change][ymid];
						avg = (A+B+D)/3;
					}
					else if(left-change < 0)
					{
						A = terrain[left][ymid+change];
						B = terrain[left][ymid-change];
						C = terrain[left+change][ymid];
						//D = terrain[zero-mid][mid];
						avg = (A+B+C)/3;
					}
					else
					{
						A = terrain[left][ymid+change];
						B = terrain[left][ymid-change];
						C = terrain[left+change][ymid];
						D = terrain[left-change][ymid];
						avg = (A+B+C+D)/4;
					}
					terrain[left][ymid] = (float) (avg + Rand(displacement));
				}
			}
			displacement *= Math.pow(2, -roughness); //displacement adjuster to add roughness(I dont know why but its important)
		}
		smooth(0.75f);
		smooth(0.75f);
		smooth(0.75f);
		smooth(0.75f);
		smooth(0.75f);
		return terrain;
	}
	
	private void initEdges()
	{
		int L = (int)length-1;
		terrain[0][0] = 0;
		terrain[0][L] = 0;
		terrain[L][0] = 0;
		terrain[L][L] = 0;
	}
	
	private void smooth(float roughness)
	{
		int len = terrain.length;
		/* Rows, left to right */
		for(int x = 1;x < len; x++)
		{
		    for (int z = 0;z < len; z++)
		    {
			terrain[x][z] = terrain[x-1][z] * (1-roughness) + 
				      terrain[x][z] * roughness;
		    }
		}

		/* Rows, right to left*/
		for(int x = len-2;x < -1; x--)
		{
		    for (int z = 0;z < len; z++)
		    {
			terrain[x][z] = terrain[x+1][z] * (1-roughness) + 
				      terrain[x][z] * roughness;
		    }
		}

		/* Columns, bottom to top */
		for(int x = 0;x < len; x++)
		{
		    for (int z = 1;z < len; z++)
		    {
			terrain[x][z] = terrain[x][z-1] * (1-roughness) + 
				      terrain[x][z] * roughness;
		    }
		}

		/* Columns, top to bottom */
		for(int x = 0;x < len; x++)
		{
		    for (int z = len; z < -1; z--)
		    {
			terrain[x][z] = terrain[x][z+1] * (1-roughness) + 
				      terrain[x][z] * roughness;
		    }
		}

	}
	
	private double Rand(double d)
	{
		Random rnd = new Random();
		double min = -displacement;
		double max = displacement;
		
		double range = max - min;
		double scaled = rnd.nextDouble() * range;
		double shifted = scaled + min;
		return shifted; // == (rand.nextDouble() * (max-min)) + min;

	}
	
	public float[][] getMap()
	{
		return terrain;
	}
	

}
