package terrain;

import org.newdawn.slick.opengl.Texture;

public class Material {


    @Override
    public String toString() {
        return "Material{" +
                "specularCoefficient=" + specularCoefficient +
                ", ambientColour=" + ambientColour +
                ", diffuseColour=" + diffuseColour +
                ", specularColour=" + specularColour +
                '}';
    }


    /** Between 0 and 1000. */
    public float specularCoefficient = 100;
    public float[] ambientColour = {0.2f, 0.2f, 0.2f};
    public float[] diffuseColour = {0.3f, 1, 1};
    public float[] specularColour = {1, 1, 1};
    public Texture texture;
}