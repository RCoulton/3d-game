package terrain;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.vector.Vector3f;


public class ChunkLoader
{
	private DiamondSquare ds;
	private int iterations;
	private double roughness;
	private int displacement;
	
	public ChunkLoader(int iterations, double roughness, int displacement) 
	{
		this.iterations = iterations;
		this.roughness = roughness;
		this.displacement = displacement;
	}

	public Chunk generateChunk(Chunk m)
	{
		//set a new diamondsquare algorithm loader
		ds = new DiamondSquare(iterations, roughness, displacement);
		//generate the terrain using the algorithm
		float[][] terrain = ds.generate();
		m.setLen(terrain); //Putting this 2d float array in the chunk
		//sets the length of the terrain
		int len = terrain.length;
		
		//loop through the amount of vertices in the terrain array
		for(int z = 0; z < len; z++)
		{
			for(int x = 0; x < len; x++)
			{
				//assign each value in the terrain array to the vertices list of the chunk
				m.getVertices().add(new Vector3f(x, terrain[x][z], z));
				//m.getVertices().add(new Vector3f(x,0,z));
			}
		}
		//loop through the terrain array
		for(int i = 0; i < len-1; i++)
		{
			for(int j = 0; j < len-1; j++)
			{
				//set faces using simple algorithm to find adjacent vertices
				int a = i * len + j;
				int b = i * len + j + len;
				setFaces(a, b, m);
			}
		}
		//generate normals for the vertices
		genVertexNormals(m);
		
		//return generated chunk
		return m;
	}
	
	private static void setFaces(int a, int b, Chunk m)
	{
		
		int[] vertexIndicesArray = new int[3];
		
		
		//Top triangle of the square of points
		vertexIndicesArray[0] = a;
		vertexIndicesArray[1] =	a+1;
		vertexIndicesArray[2] = b;
		
		Vector3f vertA = m.getVertices().get(vertexIndicesArray[0]);
		Vector3f vertB = m.getVertices().get(vertexIndicesArray[1]);
		Vector3f vertC = m.getVertices().get(vertexIndicesArray[2]);

		Vector3f u = new Vector3f(vertB.x - vertA.x, vertB.y - vertA.y, vertB.z - vertA.z);
		Vector3f v = new Vector3f(vertC.x - vertA.x, vertC.y - vertA.y, vertC.z - vertA.z);
		
		Vector3f normal = new Vector3f();
		normal.x = (u.y * v.z) - (u.z * v.y);
		normal.y = (u.z * v.x) - (u.x * v.z);
		normal.z = (u.x * v.y) - (u.y * v.x);
		normal.normalise();
		//add the top face
		m.getFaces().add(new Face(vertexIndicesArray, normal));
		
		//nullify variables just in case
		vertexIndicesArray = null;
		vertA = null;
		vertB = null;
		vertC = null;
		u = null;
		v = null;
		normal = null;
		
		//Bottom triangle of the square of points
		vertexIndicesArray = new int[3];
		
		vertexIndicesArray[0] = a+1;
		vertexIndicesArray[1] =	b;
		vertexIndicesArray[2] = b+1;
		
		vertA = m.getVertices().get(vertexIndicesArray[0]);
		vertB = m.getVertices().get(vertexIndicesArray[1]);
		vertC = m.getVertices().get(vertexIndicesArray[2]);

		u = new Vector3f(vertB.x - vertA.x, vertB.y - vertA.y, vertB.z - vertA.z);
		v = new Vector3f(vertC.x - vertA.x, vertC.y - vertA.y, vertC.z - vertA.z);
		
		normal = new Vector3f();
		normal.x = (u.y * v.z) - (u.z * v.y);
		normal.y = (u.z * v.x) - (u.x * v.z);
		normal.z = (u.x * v.y) - (u.y * v.x);
		normal.normalise();

		m.getFaces().add(new Face(vertexIndicesArray, normal));
		
	}
	
	private static void genVertexNormals(Chunk m)
	{
		int i = 0;
		
		for(@SuppressWarnings("unused") Vector3f vertex:m.getVertices())
		{
			Vector3f normal = new Vector3f(0,0,0);
			for(Face face:m.getFaces())
			{
				for(int index:face.getVertexIndices())
				{
					if(index == i)
					{
						normal.x += face.getNormal().x;
						normal.y += face.getNormal().y;
						normal.z += face.getNormal().z;
					}
				}
			}
			i++;
			if(normal.length() != 0)
			{
				normal.normalise();
			}
			m.getNormals().add(normal);
		}
	}
}