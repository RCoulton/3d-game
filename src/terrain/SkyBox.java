package terrain;

import static org.lwjgl.opengl.GL11.GL_RGBA;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Vector;

import loaders.ImageLoader;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL21;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import static org.lwjgl.opengl.GL11.*;

import camera.EulerCamera;

public class SkyBox {

        private Vector<Texture> textures;
        private EulerCamera cam;
        float boxSize = 50f;
        float texSize = 1f;
        float multipier = 1f;
        
        public SkyBox(EulerCamera cam) 
        {
                this.cam = cam;
        }
        
        public void draw() {
            	glPushMatrix();               
                glPushAttrib(GL_ENABLE_BIT);
                
                glLoadIdentity();   
                GL13.glActiveTexture(GL13.GL_TEXTURE0);
                glEnable(GL_TEXTURE_2D);
//                glDepthRange(0, 1);
                
              //load textures
                textures = new Vector<Texture>();
                try 
                {                   	
                    textures.add(ImageLoader.loadTexture("Sky/front", "PNG"));
                    textures.add(ImageLoader.loadTexture("Sky/left", "PNG"));
                    textures.add(ImageLoader.loadTexture("Sky/back", "PNG"));
                    textures.add(ImageLoader.loadTexture("Sky/right", "PNG"));
                    textures.add(ImageLoader.loadTexture("Sky/top", "PNG"));
                    textures.add(ImageLoader.loadTexture("Sky/bottom", "PNG"));
                }
                catch (Exception e)
                {
                	System.out.println(e.getStackTrace().toString());
                	System.out.println(e.getMessage());
                	System.out.println(e.getCause());
                	System.out.println(e.getLocalizedMessage());
                }                
                
                glRotatef(-cam.roll() + 180,  0, 0, -1);
                glRotatef(-cam.pitch(), 1, 0, 0);
                glRotatef(-cam.yaw(),   0, 1, 0);
//                glScalef(15,15, 15);
           
                // Just in case set all vertices to white.
            glColor4f(1,1,1,1);

            // Render the front quad
            textures.get(0).bind();
            clampToEdge();
            glBegin(GL_QUADS);
            	glTexCoord2f(0, 0);
                    glVertex3f(  -boxSize * multipier , boxSize * multipier , -boxSize );
                glTexCoord2f(1, 0);
                    glVertex3f( boxSize * multipier , boxSize * multipier , -boxSize );
                glTexCoord2f(1, 1);
                    glVertex3f( boxSize * multipier,  -boxSize * multipier, -boxSize );
                glTexCoord2f(0, 1);
                    glVertex3f(  -boxSize * multipier,  -boxSize * multipier, -boxSize );
            glEnd();

            // Render the left quad
            textures.get(1).bind();
            clampToEdge();
            glBegin(GL_QUADS);
            	glTexCoord2f(0, 0); 
            		glVertex3f(  -boxSize , boxSize * multipier, boxSize * multipier);
            	glTexCoord2f(1, 0); 
            		glVertex3f( -boxSize , boxSize * multipier, -boxSize * multipier);
            	glTexCoord2f(1, 1); 
            		glVertex3f( -boxSize ,  -boxSize * multipier, -boxSize * multipier);
            	glTexCoord2f(0, 1); 
            		glVertex3f(  -boxSize ,  -boxSize * multipier, boxSize * multipier);
            glEnd();
            
            // Render the back quad
            textures.get(2).bind();
            clampToEdge();
            glBegin(GL_QUADS);
            	glTexCoord2f(0, 0); 
            		glVertex3f(  boxSize * multipier, boxSize * multipier, boxSize );
            	glTexCoord2f(1, 0); 
            		glVertex3f( -boxSize * multipier, boxSize * multipier, boxSize );
            	glTexCoord2f(1, 1); 
            		glVertex3f( -boxSize * multipier,  -boxSize * multipier, boxSize );
            	glTexCoord2f(0, 1); 
            		glVertex3f(  boxSize * multipier,  -boxSize * multipier, boxSize );
            glEnd();
            
            // Render the right quad
            textures.get(3).bind();
            clampToEdge();
            glBegin(GL_QUADS);
            	glTexCoord2f(0, 0); 
            		glVertex3f(  boxSize , boxSize * multipier, -boxSize * multipier);
            	glTexCoord2f(1, 0); 
            		glVertex3f( boxSize , boxSize * multipier, boxSize * multipier);
            	glTexCoord2f(1, 1); 
            		glVertex3f( boxSize ,  -boxSize * multipier, boxSize * multipier);
            	glTexCoord2f(0, 1); 
            		glVertex3f(  boxSize ,  -boxSize * multipier, -boxSize * multipier);
            glEnd();
            
            // Render the top quad
            textures.get(4).bind();
            clampToEdge();
            glBegin(GL_QUADS);
            	glTexCoord2f(0, 0); 
            		glVertex3f(  -boxSize * multipier, -boxSize , boxSize * multipier);
            	glTexCoord2f(1, 0); 
            		glVertex3f( boxSize * multipier, -boxSize , boxSize * multipier);
            	glTexCoord2f(1, 1); 
            		glVertex3f( boxSize * multipier,  -boxSize , -boxSize * multipier);
            	glTexCoord2f(0, 1); 
            		glVertex3f(  -boxSize * multipier,  -boxSize , -boxSize * multipier);
            glEnd();
            
            // Render the bottom quad
            textures.get(5).bind();
            clampToEdge();
            glBegin(GL_QUADS);
            	glTexCoord2f(0, 0); 
            		glVertex3f(  -boxSize * multipier, boxSize , -boxSize * multipier);
            	glTexCoord2f(1, 0); 
            		glVertex3f( boxSize * multipier, boxSize , -boxSize * multipier);
            	glTexCoord2f(1, 1); 
            		glVertex3f( boxSize * multipier,  boxSize , boxSize * multipier);
            	glTexCoord2f(0, 1); 
            		glVertex3f(  -boxSize * multipier,  boxSize , boxSize * multipier);
            glEnd();
                     
            // Restore enable bits and matrix
//            glDepthRange(0, 1);
            glPopAttrib();            
            glPopMatrix();

        }

        //clamp textures, that edges get don't create a line in between
        private void clampToEdge() {
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL12.GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL12.GL_CLAMP_TO_EDGE);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
                glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        }
}