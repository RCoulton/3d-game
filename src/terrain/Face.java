package terrain;
import org.lwjgl.util.vector.Vector3f;

public class Face {

	
	private final int[] vertexIndices = {-1, -1, -1};
    private final int[] normalIndices = {-1, -1, -1};
    private Vector3f normal;

    
    public Face(int[] vertexIndices, Vector3f normal) {
        this.vertexIndices[0] = vertexIndices[0];
        this.vertexIndices[1] = vertexIndices[1];
        this.vertexIndices[2] = vertexIndices[2];
        this.normal = normal;
    }


    public Face(int[] vertexIndices, int[] normalIndices) {
        this.vertexIndices[0] = vertexIndices[0];
        this.vertexIndices[1] = vertexIndices[1];
        this.vertexIndices[2] = vertexIndices[2];
        this.normalIndices[0] = normalIndices[0];
        this.normalIndices[1] = normalIndices[1];
        this.normalIndices[2] = normalIndices[2];
    }
    
    public Vector3f getNormal()
    {
    	return normal;
    }
    
    public int[] getVertexIndices() {
        return vertexIndices;
    }


    public int[] getNormalIndices() {
        return normalIndices;
    }
}