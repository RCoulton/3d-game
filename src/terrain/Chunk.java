package terrain;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.opengl.GL13;
import org.lwjgl.opengl.GL14;
import org.lwjgl.opengl.GL15;
import org.lwjgl.opengl.GL20;
import org.lwjgl.opengl.GL32;
import org.lwjgl.opengl.Pbuffer;
import org.lwjgl.util.Rectangle;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;
import org.lwjgl.util.vector.Vector4f;
import org.newdawn.slick.opengl.PNGDecoder;
import org.newdawn.slick.opengl.Texture;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.*;
import static org.lwjgl.opengl.GL15.*;

import utility.BufferTools;

public class Chunk{

    private final List<Vector3f> vertices = new ArrayList<Vector3f>();
    private final List<Vector2f> textureCoordinates = new ArrayList<Vector2f>();
    private final List<Vector3f> normals = new ArrayList<Vector3f>();
    private final List<Face> faces = new ArrayList<Face>();
    private final Vector2f location;
    private final int[] adjChunkIndices = {-1,-1};
    private final HashMap<String, Material> materials = new HashMap<String, Material>();
    private boolean enableSmoothShading = true;
    private Rectangle bound = new Rectangle();
    private int displayList;
    public boolean hasList = false;
    public boolean generated = false;
    
    private float[][] len;    
	
	private static int numTextures = 3;
    
    private static int[] textures = new int[numTextures];
    
    /* Vertex and Normal VBO handles */
    public int vboVertexHandle;
    public int vboNormalHandle;
    
    /* These are some information holders, can probably exclude them later since we already
     * have some variables or parameters that are given the information */
//    private Vector3f[] m_position;
    private Vector4f[] m_color;
//    private Vector3f[] m_normal;
    private Vector2f[] m_tex0;
    private int[] m_index;
    
    /* Make some VBO location holders, without these we cannot reference the buffers */
	int vertBuffer = 0, normBuffer = 0, colBuffer = 0, tex0Buffer = 0, tex1Buffer = 0, tex2Buffer = 0,indexBuffer = 0;
   
    
    public Chunk(Vector2f location, int[] adjChunkIndices, int displayList, int chunkSize)
    {
    	this.location = location;
    	this.displayList = displayList;
    	this.adjChunkIndices[0] = adjChunkIndices[0];
    	this.adjChunkIndices[1] = adjChunkIndices[1];
    	
    	//sets the bound of the chunk
    	bound = new Rectangle((int)location.x, (int)location.y, chunkSize, chunkSize);
    }
    
    public Chunk()
    {
    	this.location =  new Vector2f(0,0);
    	this.bound = new Rectangle((int)location.x, (int)location.y, 32, 32);
    }

	public void setUpVBO()
	{
		/* size the m_holders */
//		m_position = new Vector3f[faces.size() * 9];// not sure if this is the height * width??		
		m_color = new Vector4f[faces.size() * 9];
//		m_normal = new Vector3f[faces.size() * 9];
		m_tex0 = new Vector2f[faces.size() * 9];
		
		//set up the handles
		vboVertexHandle= glGenBuffers();
		vboNormalHandle= glGenBuffers();
        
        //set up the buffers
        FloatBuffer verticesBuffer = BufferTools.reserveData(faces.size() * 9);
        FloatBuffer normalsBuffer = BufferTools.reserveData(faces.size() * 9);     		
                
        //loop through every face in the chunk
        for (Face face : faces) {
        	verticesBuffer.put(BufferTools.asFloats(vertices.get(face.getVertexIndices()[0])));
        	verticesBuffer.put(BufferTools.asFloats(vertices.get(face.getVertexIndices()[1])));
        	verticesBuffer.put(BufferTools.asFloats(vertices.get(face.getVertexIndices()[2])));
        	
        	normalsBuffer.put(BufferTools.asFloats(normals.get(face.getVertexIndices()[0])));
        	normalsBuffer.put(BufferTools.asFloats(normals.get(face.getVertexIndices()[1])));
        	normalsBuffer.put(BufferTools.asFloats(normals.get(face.getVertexIndices()[2])));
        }
       // Vector3f n1 = normals.get(face.getVertexIndices()[0]);
        //glNormal3f(n1.x, n1.y, n1.z);
        
        verticesBuffer.flip();
        normalsBuffer.flip();
        
        
        glBindBuffer(GL_ARRAY_BUFFER, vboVertexHandle);
        glBufferData(GL_ARRAY_BUFFER, verticesBuffer, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, vboNormalHandle);
        glBufferData(GL_ARRAY_BUFFER, normalsBuffer, GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        if (len.length > 0)
        {        	
        	int numTriangles = (len.length - 1) * (len.length - 1) * 2;
        	
        	m_index = new int[numTriangles * 3];
        	int index = 0;
        	for (int j = 0; j < (len.length - 1); ++j)
        	{
        		for (int i = 0; i < (len.length - 1); ++i)
        		{
        			int vertexIndex = (j * len.length) + i;
        			//Top Triangle
        			m_index[index++] = vertexIndex;
        			m_index[index++] = vertexIndex + (len.length / 2) + 1;
        			m_index[index++] = vertexIndex + 1;
        			//Bottom Triangle
        			m_index[index++] = vertexIndex;
        			m_index[index++] = vertexIndex + (len.length / 2);
        			m_index[index++] = vertexIndex + (len.length / 2) + 1;
        		}
        	} 
        	
        	for (int j = 0; j < (len.length); ++j)
        	{
        		for (int i = 0; i < (len.length); ++i)
        		{
        			float height = len[i][j];
        			
        			int colIndex = (j * len.length) + i;
        			
        			float S = ( i / (float)(len.length - 1) );
                    float T = ( j / (float)(len.length - 1) );
         
                    float X = ( S * len.length ) - ((float)len.length / 2);
                    float Y = height;
                    float Z = ( T * len.length ) - ((float)len.length / 2);
                    
                    float tex0con = 1.0f - getPercentage(height, 0.0f, 0.75f);
                    float tex2con = 1.0f - getPercentage(height, 0.75f, 1.0f);
                    m_color[colIndex] = new Vector4f(tex0con,tex0con,tex0con,tex2con);
                    m_tex0[colIndex] = new Vector2f(S,T);
        		}
        	} 
        }
        
        /* create the buffers for drawing */
		vertBuffer = glGenBuffers();
		normBuffer = glGenBuffers();
		colBuffer = glGenBuffers();
		tex0Buffer = glGenBuffers();
		tex1Buffer = glGenBuffers();
		tex2Buffer = glGenBuffers();
		indexBuffer = glGenBuffers();
		
		glBindBuffer( GL_ARRAY_BUFFER, colBuffer );
	    glBufferData( GL_ARRAY_BUFFER, m_color.length, GL_STATIC_DRAW );
		
		/* setup the textures and give the buffers the intel */
		textures[0]=setupTextures("res/textures/Grass.png");
		
        glBindBuffer(GL_ARRAY_BUFFER, tex0Buffer);
        glBufferData(GL_ARRAY_BUFFER, textures[0], GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        textures[1]=setupTextures("res/textures/Dirt.png");
        
        glBindBuffer(GL_ARRAY_BUFFER, tex1Buffer);
        glBufferData(GL_ARRAY_BUFFER, textures[1], GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        
        textures[2]=setupTextures("res/textures/Stone.png");
        
        glBindBuffer(GL_ARRAY_BUFFER, tex2Buffer);
        glBufferData(GL_ARRAY_BUFFER, textures[2], GL_DYNAMIC_DRAW);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
                        
        glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indexBuffer );
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_index.length, GL_STATIC_DRAW);
        
        glBindBuffer( GL_ARRAY_BUFFER, colBuffer );
        glBufferData(GL_ARRAY_BUFFER, m_color.length, GL_STATIC_DRAW);
        
        
//        System.out.println(textures[0]);
//        System.out.println(textures[1]);
//        System.out.println(textures[2]);
//        System.out.println(tex2Buffer);
        
        hasList = true;

	}
	
	private FloatBuffer createFloatB(int x, int y, List<Vector3f> vert)
	{		
		FloatBuffer output = BufferUtils.createFloatBuffer(x * y);
		float[] verts = new float[vert.size() * 3];
		int index = 0;
		for (Vector3f ve: vert)
		{
			for (int i = 0; i < 3; i++)
			{
				if (i == 0) 
					{
						verts[index] = ve.x;
						index++;
					}
				if (i == 1) 
					{
						verts[index] = ve.y;
						index++;
					}
				if (i == 2) 
					{
						verts[index] = ve.z;
						index++;
					}
			}
		}
		output.put(verts);
		output.flip();
		return output;
	}
	
	public void drawChunk()
	{
		ByteBuffer off = BufferUtils.createByteBuffer(1);
		
		glMatrixMode( GL_MODELVIEW );
		//Below is suppose to take a float? Ref code:
		//glMultMatrixf( glm::value_ptr(m_LocalToWorldMatrix) );
//		glMultMatrix(m);
		
		/* Setting up texture stage 0 (or bottom texture) */
		glActiveTexture( GL_TEXTURE0 );
		glMatrixMode( GL_TEXTURE );
		
//		glScalef(32.0f, 32.0f, 1.0f); scaling out for now
		
		glEnable( GL_TEXTURE_2D );
		glBindTexture(GL_TEXTURE_2D, textures[0]);
		glClientActiveTexture( GL_TEXTURE0 );
		glEnableClientState( GL_TEXTURE_COORD_ARRAY );
		glBindBuffer( GL_ARRAY_BUFFER, tex0Buffer);
		glTexCoordPointer(2, GL_FLOAT, 0, 0 );
		
	    // Disable lighting because it changes the primary color of the vertices that are
	    // used for the multitexture blending.
	    glDisable( GL_LIGHTING );
	 
	    // Texture Stage 1
	    // Perform a linear interpolation between the output of stage 0
	    // (i.e texture0) and texture1 and use the RGB portion of the vertex's
	    // color to mix the two.
	    glActiveTexture(GL_TEXTURE1 );
	    glMatrixMode( GL_TEXTURE );
	    
//	    glScalef( 32.0f, 32.0f , 1.0f );
	 
	    glEnable( GL_TEXTURE_2D );
	    glBindTexture( GL_TEXTURE_2D, textures[1] );
	 
	    glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE );
	    glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_INTERPOLATE );
	 
	    glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_PREVIOUS );
	    glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR );
	 
	    glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_TEXTURE );
	    glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR );
	 
	    glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB, GL_PRIMARY_COLOR );
	    glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_COLOR );
	 
	    glClientActiveTexture(GL_TEXTURE1);
	    glEnableClientState( GL_TEXTURE_COORD_ARRAY );
	    glBindBuffer( GL_ARRAY_BUFFER, tex1Buffer );
	    glTexCoordPointer(2, GL_FLOAT, 0, 0 );
	    
	    // Texture Stage 2
	    // Perform a linear interpolation between the output of stage 1
	    // (i.e texture0 mixed with texture1) and texture2 and use the ALPHA
	    // portion of the vertex's color to mix the two.
	    glActiveTexture( GL_TEXTURE2 );
	    glMatrixMode( GL_TEXTURE );
	    
//	    glScalef( 32.0f, 32.0f , 1.0f );
	     
	    glEnable( GL_TEXTURE_2D );
	    glBindTexture( GL_TEXTURE_2D, textures[2] );
	     
	    glTexEnvi( GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_COMBINE );
	    glTexEnvi( GL_TEXTURE_ENV, GL_COMBINE_RGB, GL_INTERPOLATE );
	     
	    glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE0_RGB, GL_PREVIOUS );
	    glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND0_RGB, GL_SRC_COLOR );
	     
	    glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE1_RGB, GL_TEXTURE );
	    glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND1_RGB, GL_SRC_COLOR );
	     
	    glTexEnvi( GL_TEXTURE_ENV, GL_SOURCE2_RGB, GL_PRIMARY_COLOR );
	    glTexEnvi( GL_TEXTURE_ENV, GL_OPERAND2_RGB, GL_SRC_ALPHA );
	    
	    glClientActiveTexture(GL_TEXTURE2);
	    glEnableClientState( GL_TEXTURE_COORD_ARRAY );
	    glEnableClientState(GL_ARRAY_BUFFER);
	    glBindBuffer( GL_ARRAY_BUFFER, tex2Buffer );
	    glTexCoordPointer(2, GL_FLOAT, 0, 0 );
	    
	    //Rendering
	    glEnableClientState( GL_INDEX_ARRAY );
	    glEnableClientState( GL_VERTEX_ARRAY );
	    glEnableClientState( GL_COLOR_ARRAY );
	    glEnableClientState( GL_NORMAL_ARRAY );
	     
	    glBindBuffer( GL_ARRAY_BUFFER, vboVertexHandle );
	    glVertexPointer( 3, GL_FLOAT, 0, 0 );
	    glBindBuffer( GL_ARRAY_BUFFER, colBuffer );
	    glColorPointer( 4, GL_FLOAT, 0, 0 );
	    glBindBuffer( GL_ARRAY_BUFFER, vboNormalHandle );
	    glNormalPointer( GL_FLOAT, 0, 0 );
	    
	    /* I need a color buffer above and somehow need and index buffer. */
	    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, indexBuffer );
//	    glDrawElements( GL_TRIANGLES, off );
//	    glDrawElements(GL_TRIANGLES, m_index.length, GL_UNSIGNED_INT, (long)0);
	    glDrawArrays(GL_TRIANGLES, 0, m_index.length); //Works but causes crashes and weird rendering
	    
	    //Cleanup 
	    glDisableClientState( GL_NORMAL_ARRAY );
	    glDisableClientState( GL_COLOR_ARRAY );
	    glDisableClientState( GL_VERTEX_ARRAY );
	    glDisableClientState( GL_INDEX_ARRAY );
	 
	    glActiveTexture(GL_TEXTURE2);
	    
	    glDisable(GL_TEXTURE_2D);
	    glClientActiveTexture(GL_TEXTURE2);
	    glDisableClientState( GL_TEXTURE_COORD_ARRAY );
	 
	    glActiveTexture(GL_TEXTURE1);
	    
	    glDisable(GL_TEXTURE_2D);
	    glClientActiveTexture(GL_TEXTURE1);
	    glDisableClientState( GL_TEXTURE_COORD_ARRAY );
 
	    glActiveTexture(GL_TEXTURE0);
	    
	    glDisable(GL_TEXTURE_2D);
	    glClientActiveTexture(GL_TEXTURE0);
	    glDisableClientState( GL_TEXTURE_COORD_ARRAY );
	 
	    glBindBuffer( GL_ARRAY_BUFFER, 0 );
	    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
	 
	    glMatrixMode( GL_MODELVIEW );
	}
		
	private static int setupTextures(String filename)
    {
    	IntBuffer tmp=BufferUtils.createIntBuffer(1);
    	GL11.glGenTextures(tmp);
    	tmp.rewind();
    	try
    	{
    		InputStream in = new FileInputStream(filename);
    		PNGDecoder decoder=new PNGDecoder(in);
    		ByteBuffer data =ByteBuffer.allocateDirect(4*decoder.getWidth()*decoder.getHeight());
    		decoder.decode(data, decoder.getWidth()*4, PNGDecoder.Format.RGBA);
    		data.rewind();
    		GL11.glBindTexture(GL11.GL_TEXTURE_2D, tmp.get(0));
    		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MAG_FILTER, GL11.GL_NEAREST);
    		GL11.glTexParameteri(GL11.GL_TEXTURE_2D, GL11.GL_TEXTURE_MIN_FILTER, GL11.GL_NEAREST);
    		GL11.glTexImage2D(GL11.GL_TEXTURE_2D, 0, GL_RGBA, decoder.getWidth(), decoder.getHeight(), 0, GL_RGBA, GL11.GL_UNSIGNED_BYTE, data);
    		GL11.glPixelStorei(GL11.GL_UNPACK_ALIGNMENT, 4);
    	}
    	catch (FileNotFoundException ex)
    	{
    		System.out.println("Error " + filename + " not found");
    	}
    	catch (IOException e)
    	{
    		System.out.println("Error decoding " + filename);
    	}
    	tmp.rewind();
    	return tmp.get(0);
    }

    public void enableStates() {
        if (hasTextureCoordinates()) {
            glEnable(GL_TEXTURE_2D);
        }
        if (isSmoothShadingEnabled()) {
            glShadeModel(GL_SMOOTH);
        } else {
            glShadeModel(GL_FLAT);
        }
    }
    
    //Helper Methods
    private static void deleteVertBuffer(int ID)
	{
		if (ID != 0)
		{
			glDeleteBuffers(ID);
			ID = 0;
		}
	}
	
	private static int createVertBuffer(int ID)
	{
		deleteVertBuffer(ID);
		ID = glGenBuffers();
		return ID;
	}
	
	private static void deleteTexture(int ID)
	{
		if (ID != 0)
		{
			glDeleteTextures(ID);
			ID = 0;
		}
	}
	
	private static void createTexture(int ID)
	{
		deleteTexture(ID);
		ID = glGenTextures();
	}
	
	private float getPercentage(float value, float min, float max)
	{
		float returnValue = (value - min) / (max - min);
		if (returnValue <= min)
		{
			return 0f;
		}
		else
		{
			return 1f;
		}
	}

	//Sets
    public void setSmoothShadingEnabled(boolean smoothShadingEnabled) {
        this.enableSmoothShading = smoothShadingEnabled;
    }
    
    public void setLen(float[][] len) {
		this.len = len;
	}    
	
	//Gets
    public boolean isGenerated() {
		return generated;
	}
    
    public int adjChunkIndex(int i)
    {
    	return adjChunkIndices[i];
    }

	public void setGenerated(boolean generated) {
		this.generated = generated;
	}

	public int getDisplayList() 
    {
		return displayList;		
	}
	
    public boolean hasTextureCoordinates() {
        return getTextureCoordinates().size() > 0;
    }


    public boolean hasNormals() {
        return getNormals().size() > 0;
    }


    public List<Vector3f> getVertices() {
        return vertices;
    }


    public List<Vector2f> getTextureCoordinates() {
        return textureCoordinates;
    }


    public List<Vector3f> getNormals() {
        return normals;
    }


    public List<Face> getFaces() {
        return faces;
    }
    
    public boolean isSmoothShadingEnabled() {
        return enableSmoothShading;
    }
    
    public HashMap<String, Material> getMaterials() {
        return materials;
    }

    public Rectangle getBound() {
		return bound;
	}
    
    public float[][] getLen() {
		return len;
	}
    
    //Other
    
    public void setBound(Rectangle bound) {
		this.bound = bound;
	}

    public Vector2f getLocation() {
		return location;
	}

	public static class Material 
    {
    	/** Between 0 and 1000. */
        public float specularCoefficient = 100;
        public float[] ambientColour = {0.2f, 0.2f, 0.2f};
        public float[] diffuseColour = {0.3f, 1, 1};
        public float[] specularColour = {1, 1, 1};
        public Texture texture;
        
    	@Override
    	public String toString() {
        	return "Material{" +
                    "specularCoefficient=" + specularCoefficient +
                    ", ambientColour=" + ambientColour +
                    ", diffuseColour=" + diffuseColour +
                    ", specularColour=" + specularColour +
                    '}';
        }
    }
	
	public float getHeight(Vector2f loc)
	{
		for(Vector3f vertex:vertices)
		{
			if(Math.round(loc.x) == vertex.x && Math.round(loc.y) == vertex.z)
			{
				return vertex.y;
			}
		}
		return -1;
	}
}
