package terrain;

import static org.lwjgl.opengl.GL11.glCallList;
import static org.lwjgl.opengl.GL11.glGenLists;
import static org.lwjgl.opengl.GL11.*;

import java.util.ArrayList;
import java.util.List;

import org.lwjgl.util.Rectangle;
import org.lwjgl.util.vector.Vector2f;
import org.lwjgl.util.vector.Vector3f;

import utility.ShaderLoader;



public class Terrain 
{
	private List<Chunk> chunks = new ArrayList<Chunk>();
	private float chunkSize;
	private int numOfChunks;
	private ChunkLoader chunkLoader;
	
	public Terrain(int iterations, double roughness, int displacement, int numOfChunks)
	{
		this.numOfChunks = numOfChunks;
		//creates a chunk loader that will make all chunks the same style
		chunkLoader = new ChunkLoader(iterations, roughness, displacement);
		//assigns the size of each chunk
		chunkSize = (float) (Math.pow(2, iterations));
		//size of chunk array(one directional)
		int len = (int) Math.sqrt((double)numOfChunks);
		
		//Creates enough lists to hold every chunk
		int base = glGenLists(numOfChunks);
		int temp = 0;
		
		
		//For loop iterates through the amount of chunks
		//Push i and j half length to the negatives so the chunks are centered around (0,0)
		for(int i=-(len/2); i < (len/2); i++)
		{
			for(int j=-(len/2); j < (len/2); j++)
			{
				//finds the index of the chunk to the right and below
				int[] adjChunkIndices = {temp+1, temp+len};
				//adds a new chunk to the chunk list
				//new vector2f is for positioning
				//adjChunkIndices is for the adjacent
				//base+temp sets the display list identifier
				//chunkSize is the size of the chunks
				chunks.add(new Chunk(new Vector2f(j*chunkSize,i*chunkSize), adjChunkIndices, base+temp, (int)chunkSize));
				//increase temp for next display list
				temp++;
			}
		}
		
	}

	public void draw(Rectangle bound)
	{
		//Loop through every chunk
		for(Chunk chunk:chunks)
		{
			//if the chunk is within view distance
			if(chunk.getBound().intersects(bound))
			{
				//if the chunk is generated
				if(chunk.isGenerated())
				{
					//if the display list is generated
					if(chunk.hasList)
					{
						//draw the chunk display list
						//glCallList(chunk.getDisplayList());
//						glDrawArrays(GL_TRIANGLES, 0, (int)(chunkSize*chunkSize));
						//Since we are using VBOs, the data is static, not final
						//Therefore we must do the translation on the draw function vs in the loading function
						glPushMatrix();
						//glScalef(4,4,4);
						glTranslatef(chunk.getLocation().x, 0, chunk.getLocation().y);
						
						chunk.drawChunk();
						glPopMatrix();
					}
					else
					{
						chunk.setUpVBO();
					}
				}
				else
				{
					//generate a new chunk
					chunk = chunkLoader.generateChunk(chunk);
					chunk.generated = true;
					//set the edges to match
					//matchEdges();
				}
			}
		}
	}
	
	private boolean inChunk(Rectangle bound)
	{
		for(Chunk chunk:chunks)
		{
			if(chunk.getBound().intersects(bound))
			{
				return true;
			}
		}
		return false;
	}
	
	public float getHeight(Rectangle bound, Vector2f loc)
	{
		for(Chunk chunk:chunks)
		{
			if(chunk.getBound().intersects(bound))
			{
				Vector2f chunkLoc = chunk.getLocation();
				Vector2f absoluteLoc = new Vector2f(loc.x - chunkLoc.x, loc.y - chunkLoc.y);
				return chunk.getHeight(absoluteLoc);
			}
		}
		return -1;
	}
	
	public Vector2f getChunkLoc(Rectangle bound)
	{
		for(Chunk chunk:chunks)
		{
			if(chunk.getBound().intersects(bound))
			{
				return chunk.getLocation();
			}
		}
		
		return new Vector2f(-1,-1);
	}
	
	private void matchEdges()
	{
		int tempRight = 0;
		int tempBottom = 0;
		int size = (int)chunkSize;
		for(Chunk chunk:chunks)
		{
			if(chunk.generated)
			{
				if(tempRight != Math.sqrt(numOfChunks)-1)
				{
					Chunk right = chunks.get(chunk.adjChunkIndex(0));
					if(right.generated)
					{
						for(int i = 0; i < size; i++)
						{
							int rightIndex = i * size;
							Vector3f element = chunk.getVertices().get((i+1) * size - 1);
							right.getVertices().set(rightIndex, element);
						}
						chunks.set(chunk.adjChunkIndex(0), right);
					}
				}
				else
				{
					tempRight = -1;
				}
				
				if(tempBottom < numOfChunks - Math.sqrt(numOfChunks))
				{
					Chunk bottom = chunks.get(chunk.adjChunkIndex(1));
					if(bottom.generated)
					{
						for(int i = 0; i < size; i++)
						{
							int rightIndex = i;
							Vector3f element = chunk.getVertices().get(size * size - size + i);
							bottom.getVertices().set(rightIndex, element);
						}
						chunks.set(chunk.adjChunkIndex(1), bottom);
					}
				}
			}
			tempBottom++;
			tempRight++;
		}
	}

}
