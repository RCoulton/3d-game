package interfaces;

public interface Drink
{
	public void drink (float quench);
	public void dehydrate (float dehydrate);
	public float getMaxThirst();
	public void setMaxThirst(float value);
	public float getMinThirst();
	public void setMinThirst(float value);
	public float getThirst();
	public void setThirst(float value);
}

