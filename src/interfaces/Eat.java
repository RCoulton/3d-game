package interfaces;

public interface Eat 
{
	public void eat (float satiate);
	public void starve (float starve);
	public float getMaxHunger();
	public void setMaxHunger(float value);
	public float getMinHunger();
	public void setMinHunger(float value);
	public float getHunger();
	public void setHunger(float value);
}
