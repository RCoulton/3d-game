package interfaces;

public interface Sleep 
{
	public void sleep (float rest);
	public void exhaust (float tired);
	public float getMaxFatigue();
	public void setMaxFatigue(float value);
	public float getMinFatigue();
	public void setMinFatigue(float value);
	public float getFatigue();
	public void setFatigue(float value);
}
