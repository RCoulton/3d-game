package interfaces;

public interface Health 
{
	public void heal (float heal);
	public void hurt (float hurt);
	public float getMaxHealth();
	public void setMaxHealth(float value);
	public float getHealth();
	public void setHealth(float value);
}
