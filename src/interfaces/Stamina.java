package interfaces;

public interface Stamina 
{
	public void rest (float rest);
	public void tire (float tire);
	public float getMaxStamina();
	public void setMaxStamina(float value);
	public float getMinStamina();
	public void setMinStamina(float value);
	public float getStamina();
	public void setStamina(float value);
}

