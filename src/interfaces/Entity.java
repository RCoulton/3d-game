package interfaces;

import objects.Model;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.Texture;

public interface Entity 
{
	public String getName();
	public void setName(String name);
	public void setTexture(Texture tex);
	public void setModel(Model mod);
	public Vector3f getScale();
	public void setScale (Vector3f scale);
}
