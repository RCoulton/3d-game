package HUD;

import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL12;
import org.lwjgl.util.vector.Vector2f;
import org.newdawn.slick.opengl.Texture;

import loaders.ImageLoader;

import static org.lwjgl.opengl.GL11.*;

public class HUD 
{
	private int displayWidth;
	private int displayHeight;
	private int healthValue;
	private Texture crosshair;
	private Texture healthBar;
	private Texture healthOverlay;
	private Vector2f crosshairLocation;
	private Vector2f healthBarLocation;
	
	public HUD(int displayWidth, int displayHeight)
	{
		this.displayWidth = displayWidth;
		this.displayHeight = displayHeight;
		crosshair = ImageLoader.loadTexture("hud/crosshair2","PNG");
		healthBar = ImageLoader.loadTexture("hud/healthBar2","PNG");
		healthOverlay = ImageLoader.loadTexture("hud/health", "PNG");

		
		crosshairLocation = new Vector2f(displayWidth/2 + crosshair.getImageWidth()/4, displayHeight/2);
		healthBarLocation = new Vector2f(displayWidth/2, 10);
	}
	
	public void update(int healthValue)
	{
		this.healthValue = healthValue;
	}
	
	public void draw()
	{
		drawCrosshair();
		drawHealth();
	}

	private void drawHealth() 
	{
		glEnable(GL_TEXTURE_2D);
		
		healthOverlay.bind();
		int adjustX = healthOverlay.getImageWidth();
		int adjustY = healthOverlay.getImageHeight();
		int adjustDown = 28;
		int adjustRight = 183;
		float endofLifeBar = 422;
		float maxHealth = 100f;
		float currentHealth = 100f;
		float remainingHealth = endofLifeBar * (currentHealth / maxHealth);
		
		glBegin(GL_QUADS);
	    
		glTexCoord2f(0,0);
		glVertex2f(healthBarLocation.x + adjustRight, healthBarLocation.y + adjustDown);
		
//		System.out.println(healthBarLocation.x + adjustRight);
//		System.out.println(healthBarLocation.x + adjustRight + remainingHealth);
		
		glTexCoord2f(1,0);
		glVertex2f(healthBarLocation.x + adjustRight + remainingHealth ,healthBarLocation.y + adjustDown);
		
		glTexCoord2f(1, 1);
		glVertex2f(healthBarLocation.x + adjustRight + remainingHealth,healthBarLocation.y + adjustDown + adjustY);
		
		glTexCoord2f(0,1);
		glVertex2f(healthBarLocation.x + adjustRight, healthBarLocation.y + adjustDown + adjustY);
		glEnd();	
		

		glEnable(GL_TEXTURE_2D);
		healthOverlay.bind();
		adjustX = healthOverlay.getImageWidth();
		adjustY = healthOverlay.getImageHeight();
		adjustDown = 17;
		adjustRight = 28;
		healthBar.bind();
		adjustX = healthBar.getImageWidth();
		adjustY = healthBar.getImageHeight();
		
		glBegin(GL_QUADS);
	    
		glTexCoord2f(0,0);
		glVertex2f(healthBarLocation.x, healthBarLocation.y);
		
		glTexCoord2f(1,0);
		glVertex2f(healthBarLocation.x + adjustX,healthBarLocation.y);
		
		glTexCoord2f(1, 1);
		glVertex2f(healthBarLocation.x + adjustX,healthBarLocation.y + adjustY);
		
		glTexCoord2f(0,1);
		glVertex2f(healthBarLocation.x ,healthBarLocation.y + adjustY);
		glEnd();
		

		glDisable(GL_TEXTURE_2D);
		
	}
	
	private void drawCrosshair() 
	{
		glEnable(GL_TEXTURE_2D);
		crosshair.bind();
		int adjustX = crosshair.getImageWidth()/2;
		int adjustY = crosshair.getImageHeight()/2;
		
		glBegin(GL_QUADS);
	    
		glTexCoord2f(0,0);
		glVertex2f(crosshairLocation.x - adjustX, crosshairLocation.y - adjustY);
		
		glTexCoord2f(1,0);
		glVertex2f(crosshairLocation.x + adjustX,crosshairLocation.y - adjustY);
		
		glTexCoord2f(1, 1);
		glVertex2f(crosshairLocation.x + adjustX,crosshairLocation.y + adjustY);
		
		glTexCoord2f(0,1);
		glVertex2f(crosshairLocation.x - adjustX,crosshairLocation.y + adjustY);
		glEnd();
		glDisable(GL_TEXTURE_2D);

	}	
}
