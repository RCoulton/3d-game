
import org.lwjgl.LWJGLException;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.ARBShaderObjects;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GL20;
import org.lwjgl.util.Rectangle;
import org.lwjgl.util.vector.Vector2f;



import java.io.IOException;
import java.nio.FloatBuffer;
import java.text.DecimalFormat;

import loaders.FileLoader;



import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL20.*;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.UnicodeFont;
import org.newdawn.slick.font.effects.ColorEffect;

import physics.Physics;

import HUD.HUD;
import camera.EulerCamera;
import terrain.RandomTerrain;
import terrain.Terrain;
import terrain.SkyBox;
import utility.*;


public class Main 
{       
    //FINAL GAME VARIABLES
    private static final int DISPLAY_WIDTH = 1280;
    private static final int DISPLAY_HEIGHT = 800;
    private static final boolean FULLSCREEN = false;
    private static final boolean VSYNC = true;
    private static final String TITLE = "Game";
    private static final boolean MOUSE_GRABBED = true;
    private static final int FOV = 100; //Limit on how far to look up and down Field of View
    private static final float ASPECT_RATIO = DISPLAY_WIDTH / DISPLAY_HEIGHT;
    private static final float INITIAL_CAM_X = 0;
    private static final float INITIAL_CAM_Y = 0;
    private static final float INITIAL_CAM_Z = 0;
    private static final float MOUSE_SPEED = 1;
    private static final float MAX_LOOK_UP = 80;
    private static final float MAX_LOOK_DOWN = -80;
    private static final float SPEED_X = 3;
    private static final float SPEED_Y = 5;
    private static final float SPEED_Z = 3;
    private static final int MAP_SIZE = 5;
    private static final int CHUNK_SIZE = (int) (Math.pow(2, MAP_SIZE) + 1);
    private static final int NUM_OF_CHUNKS = 1024; //must have pure root(no decimals)
    private static final double MAP_ROUGHNESS = .75;
    private static final int MAP_DISPLACEMENT = 10;
    
    
    private static final String FONT = "Times New Roman";
    private static final int FONT_SIZE = 18;
    
    

    private static EulerCamera cam;
    private static Terrain terrain;
    private static RandomTerrain rTerrain;

    private static SkyBox sky = new SkyBox(cam);
    private static HUD hud;

    private static final FloatBuffer perspectiveProjectionMatrix = BufferTools.reserveData(16);
    private static final FloatBuffer orthographicProjectionMatrix = BufferTools.reserveData(16);
    private static UnicodeFont font;
    private static boolean showDevInfo = true;
    private static int delay = 0;
    private static final DecimalFormat formatter = new DecimalFormat("#.##");
    private static int terrainShader;
    private static int otherShader;
    private static Rectangle bound = new Rectangle(0, 0 , CHUNK_SIZE *4, CHUNK_SIZE*4);
    private static Rectangle playerBound = new Rectangle(0,0,1,1);
    private static Vector2f playerLoc = new Vector2f(0,0);
    private static float height = 5;
    private static float  mapHeight = 0;
    public static float playerHeight = 5;
    private static Vector2f chunkLoc = new Vector2f(0,0);
    private static float velocityY = 0;
    private static boolean isJumping = false;
    private static float jumpDelay = 0;
    private static float fps = 0;
    
            
    public static void main(String[] args) throws IOException 
    {
    	FileLoader.LoadFiles();
        setUpDisplay();
        setUpFonts();
        setUpMap();
        setUpCamera();
        setUpShaders();
        setUpLighting();
        setUpHUD();
        while(!Display.isCloseRequested())
        {
        		logic();	
        		checkInput();
                render();
                Display.update();
                Display.sync(60);
        }
        cleanUp();
        System.exit(0);

    }

    private static void setUpHUD() 
    {
		hud = new HUD(DISPLAY_WIDTH, DISPLAY_HEIGHT);
	}

	private static void cleanUp() 
    {
        Display.destroy();
    }

    private static void logic() 
    {         	
    		fps = (float)1000/Time.getDelta();
    		playerLoc.x = cam.x();
    		playerLoc.y = cam.z();
    		bound.setX((int)playerLoc.x-CHUNK_SIZE*2);
    		bound.setY((int)playerLoc.y-CHUNK_SIZE*2);
    		playerBound.setX((int)playerLoc.x);
    		playerBound.setY((int)playerLoc.y);
    		hud.update(10);
    		chunkLoc = terrain.getChunkLoc(playerBound);
    		mapHeight = terrain.getHeight(playerBound, playerLoc);
    		height = cam.y();
    		if(jumpDelay > 0)
    			jumpDelay--;
    		
    		if(height > mapHeight+playerHeight)
    		{
    			velocityY = Physics.gravity(velocityY, fps);
    		}
    		
    		height += velocityY;
    		
    		if(height < mapHeight+playerHeight)
			{
				height = mapHeight + playerHeight;
				velocityY = 0;
			}
    		
    		
    }

    private static void checkInput() 
    {
    	
        cam.processMouse(MOUSE_SPEED, MAX_LOOK_UP, MAX_LOOK_DOWN);

        cam.processKeyboard(fps,SPEED_X, SPEED_Y, SPEED_Z);
        cam.setPosition(cam.x(), height, cam.z());        
        while(Keyboard.next())
        {
                if(Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState())
                {
                        cleanUp();
                        System.exit(0);
                }
                if(Keyboard.getEventKey() == Keyboard.KEY_F3 && Keyboard.getEventKeyState())
                {
                        if(showDevInfo && delay == 0)
                        {
                                showDevInfo = false;
                                delay = 10;
                        }
                        if(!showDevInfo && delay == 0)
                        {
                                showDevInfo = true;
                                delay = 10;
                        }
                }
                
                if(Keyboard.getEventKey() == Keyboard.KEY_SPACE && Keyboard.getEventKeyState())
                {
                	if(jumpDelay == 0)
                	{
                		if(velocityY == 0)
                		{
                			velocityY = 1.5f;
                			jumpDelay = 20;
                		}
                	}
                }
        }
        if(delay > 0)
        	delay--;
            
    }

    private static void render() 
    {
            //glPolygonMode(GL_FRONT, GL_POINT);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        
        glLoadIdentity();
        
        cam.applyTranslations();
        
        
        glLight(GL_LIGHT0, GL_POSITION, BufferTools.asFlippedFloatBuffer(cam.x(), cam.y(), cam.z(), 1));
        glUseProgram(terrainShader);
        //sky.draw();
        terrain.draw(bound);
        //rTerrain.draw(bound);
        glUseProgram(0);
        glMatrixMode(GL_PROJECTION);
        glLoadMatrix(orthographicProjectionMatrix);
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glLoadIdentity();
        glDisable(GL_LIGHTING);
        glClear(GL_DEPTH_BUFFER_BIT);
        if(showDevInfo)
        {
                font.drawString(10, 10, "Position:\nX: " + 
                		formatter.format(cam.x()) +
                        "\nY: " + 
                		formatter.format(cam.y()) +
                        "\nZ: " + 
                		formatter.format(cam.z()) +
                		"\nPitch: " +
                		formatter.format(cam.pitch()) +
                        "\nYaw: " + 
                		formatter.format(cam.yaw()) +
                        "\nRoll: " + 
                		formatter.format(cam.roll()) +
                		"\nChunk Location: " + chunkLoc.x + ", " + chunkLoc.y +
                		"\nY Velocity: " + velocityY + 
                		"\nJump Delay: " + jumpDelay + 
                		"\nFPS: " + fps);
        }
        
        hud.draw();
        glEnable(GL_LIGHTING);
        glPopMatrix();
        glMatrixMode(GL_PROJECTION);
        glLoadMatrix(perspectiveProjectionMatrix);
        glMatrixMode(GL_MODELVIEW);
            
    }

    private static void setUpLighting() 
    {
        glShadeModel(GL_SMOOTH);
        glEnable(GL_DEPTH_TEST);
        glEnable(GL_LIGHTING);
        //glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_LIGHT0);
        glLightModel(GL_LIGHT_MODEL_AMBIENT, BufferTools.asFlippedFloatBuffer(new float[]{.5f, .5f, .5f, 1f}));
        glLight(GL_LIGHT0, GL_POSITION, BufferTools.asFlippedFloatBuffer(new float[]{0, 0, 0, 1}));
        //glEnable(GL_CULL_FACE);
        //glCullFace(GL_BACK);
        glMaterialf(GL_FRONT, GL_SHININESS, 120);
        glMaterial(GL_FRONT, GL_DIFFUSE, BufferTools.asFlippedFloatBuffer(0.5f, 0.27f, 0.17f, 0));
        glClearColor(.1f, .5f, 0.7f, 1f);
            
    }

    private static void setUpShaders() 
    {
        otherShader = ShaderLoader.loadShaderPair("res/shaders/shader.vert", "res/shaders/shader.frag");
//        terrainShader = ShaderLoader.loadShaderPair("res/shaders/file.vert", "res/shaders/file.frag");
//        int sampler01=ARBShaderObjects.glGetUniformLocationARB(terrainShader, "sampler01");
//        ARBShaderObjects.glUniform1iARB(sampler01, 0);
        terrainShader = ShaderLoader.loadShaderPair("res/shaders/multiTex.vert", "res/shaders/multiTex.frag");
//        int sampler01=ARBShaderObjects.glGetUniformLocationARB(terrainShader, "sampler01");
//        ARBShaderObjects.glUniform1iARB(sampler01, 0);
//        int sampler02=ARBShaderObjects.glGetUniformLocationARB(terrainShader, "sampler02");
//        ARBShaderObjects.glUniform1iARB(sampler02, 0);
//        GL20.glUniform2f(ARBShaderObjects.glGetUniformLocationARB(terrainShader, 2), 0, 0);
        GL20.glUniform1i(ARBShaderObjects.glGetUniformLocationARB(terrainShader, "sampler01"), 0);
        GL20.glUniform1i(ARBShaderObjects.glGetUniformLocationARB(terrainShader, "sampler02"), 1);
//        GL20.glUniform1f(ARBShaderObjects.glGetUniformLocationARB(terrainShader, 2), 0);
//        ARBShaderObjects.glUniform1iARB(ARBShaderObjects.glGetUniformLocationARB(terrainShader, "sampler01"), 0);
        
        
    }

    private static void setUpCamera() 
    {
        cam = new EulerCamera(ASPECT_RATIO, INITIAL_CAM_X, INITIAL_CAM_Y, INITIAL_CAM_Z);
        cam.setFieldOfView(FOV);
        cam.applyPerspectiveMatrix();
        glGetFloat(GL_PROJECTION_MATRIX, perspectiveProjectionMatrix);
            
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, Display.getWidth(), Display.getHeight(), 0, 1, -1);
        glGetFloat(GL_PROJECTION_MATRIX, orthographicProjectionMatrix);
        
        glLoadMatrix(perspectiveProjectionMatrix);
        glMatrixMode(GL_MODELVIEW);
            
        //sky = new SkyBox(cam);
    }

    private static void setUpMap() 
    {
        terrain = new Terrain(MAP_SIZE, MAP_ROUGHNESS, MAP_DISPLACEMENT, NUM_OF_CHUNKS);
        //rTerrain = new RandomTerrain(MAP_SIZE, MAP_ROUGHNESS, MAP_DISPLACEMENT, NUM_OF_CHUNKS);
    }

    @SuppressWarnings("unchecked")
	private static void setUpFonts() 
    {
        java.awt.Font awtFont = new java.awt.Font(FONT, java.awt.Font.BOLD, FONT_SIZE);
        font = new UnicodeFont(awtFont);
        font.getEffects().add(new ColorEffect(java.awt.Color.white));
        font.addAsciiGlyphs();
        try {
                font.loadGlyphs();
        } catch (SlickException e) {
                e.printStackTrace();
                cleanUp();
        } 
    }

    private static void setUpDisplay() 
    {
        try
        {
                Display.setDisplayMode(new DisplayMode(DISPLAY_WIDTH, DISPLAY_HEIGHT));
                Display.setFullscreen(FULLSCREEN);
                Display.setVSyncEnabled(VSYNC);
                Display.setTitle(TITLE);
                Display.create();
        }
        catch(LWJGLException e)
        {
                System.err.println("The display wasn't initialize properly");
                Display.destroy();
                System.exit(1);
        }
        
        Mouse.setGrabbed(MOUSE_GRABBED);
    }
}

