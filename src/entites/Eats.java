package entites;

import interfaces.Eat;

public abstract class Eats implements Eat {
	
	protected float maxHunger = 100;
	protected float minHunger = 100;
	protected float hunger = 100;

	public Eats() {
		// TODO Auto-generated constructor stub
	}
	
	public Eats(float maxHunger, float minHunger) {
		this.maxHunger = maxHunger;
		this.minHunger = minHunger;
	}

	@Override
	public void eat(float satiate) {
		// TODO Auto-generated method stub
		if ((hunger += satiate) > maxHunger) hunger = maxHunger;
		else hunger += satiate;
	}

	@Override
	public void starve(float starve) {
		// TODO Auto-generated method stub
		if ((hunger -= starve) < 0) hunger = 0;
		else hunger -= starve;
	}

	@Override
	public float getMaxHunger() {
		// TODO Auto-generated method stub
		return this.maxHunger;
	}

	@Override
	public void setMaxHunger(float value) {
		// TODO Auto-generated method stub
		maxHunger = value;
	}

	@Override
	public float getMinHunger() {
		// TODO Auto-generated method stub
		return minHunger;
	}

	@Override
	public void setMinHunger(float value) {
		// TODO Auto-generated method stub
		minHunger = value;
	}

	@Override
	public float getHunger() {
		// TODO Auto-generated method stub
		return hunger;
	}

	@Override
	public void setHunger(float value) {
		// TODO Auto-generated method stub
		hunger = value;
	}

}
