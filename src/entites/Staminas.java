package entites;

import interfaces.Stamina;

public abstract class Staminas implements Stamina {
	
	protected float MaxStamina;
	protected float MinStamina;
	protected float Stamina;
	/*
	Ignore all of this, it was the result of a misunderstanding on my part. I commented it all out and fixed it. Commented incase it could be of use elsewhere.
	protected float BaseTireRate; //Should be near 1, used in tiring as a multiplier.
	protected float BaseTire; 
	protected float RestGainRate; 
	protected float RestGain; // Rest gained per 'x' time, before multiplied.
	*/

	public Staminas() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void rest(float rest) {
		// TODO Auto-generated method stub
		/*
		// Rest should be called every x amount of time ingame. Not every frame.
		Stamina += RestGainRate * RestGain;
		if (Stamina > MaxStamina)
		{
			Stamina = MaxStamina;
		}
		*/
		Stamina += rest;
		if (Stamina > MaxStamina)
		{
			Stamina = MaxStamina;
		}
	} 

	@Override
	public void tire(float tire) {
		// TODO Auto-generated method stub
		/*
		Stamina -= BaseTireRate * BaseTire; //We should use some kind of 'activity' multiplier as well.
		if (Stamina < MinStamina)
		{
			Stamina = MinStamina;
		}
		*/
		Stamina -= tire;
		if (Stamina <= MinStamina)
		{
			Stamina = MinStamina;
		}
	} 

	@Override
	public float getMaxStamina() {
		// TODO Auto-generated method stub
		return MaxStamina;
	}

	@Override
	public void setMaxStamina(float value) {
		// TODO Auto-generated method stub
		MaxStamina = value;
	}

	@Override
	public float getMinStamina() {
		// TODO Auto-generated method stub
		return MinStamina;
	}

	@Override
	public void setMinStamina(float value) {
		// TODO Auto-generated method stub
		MinStamina = value;
	}

	@Override
	public float getStamina() {
		// TODO Auto-generated method stub
		return Stamina;
	}

	@Override
	public void setStamina(float value) {
		// TODO Auto-generated method stub
		Stamina = value;
	}

}
