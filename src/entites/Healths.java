package entites;

import interfaces.Health;

public abstract class Healths implements Health {
	
	protected float MaxHealth;
	protected float Health;

	public Healths() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void heal(float heal) {
		// TODO Auto-generated method stub
		Health += heal;
		if (Health > MaxHealth)
		{
			Health = MaxHealth;
		}
	}

	@Override
	public void hurt(float hurt) {
		// TODO Auto-generated method stub
		Health -= hurt;
		if (Health <= 0)
		{
			Health = 0;
		}
	}

	@Override
	public float getMaxHealth() {
		// TODO Auto-generated method stub
		return MaxHealth;
	}

	@Override
	public void setMaxHealth(float value) {
		// TODO Auto-generated method stub
		MaxHealth = value;
	}

	@Override
	public float getHealth() {
		// TODO Auto-generated method stub
		return Health;
	}

	@Override
	public void setHealth(float value) {
		// TODO Auto-generated method stub
		Health = value;
	}

}
