package entites;

import interfaces.Sleep;

public abstract class Sleeps implements Sleep {
	
	protected float MaxFatigue;
	protected float MinFatigue;
	protected float Fatigue; // Level of tiredness
	protected float Exhaust;

	public Sleeps() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void sleep(float rest) {
		// TODO Auto-generated method stub
		Fatigue += rest;
		if (Fatigue > MaxFatigue)
		{
			Fatigue = MaxFatigue;
		}
	}

	@Override
	public void exhaust(float tired) {
		// TODO Auto-generated method stub
		Fatigue -= tired;
		if (Fatigue <= MinFatigue)
		{
			Fatigue = MinFatigue;
		}
	}

	@Override
	public float getMaxFatigue() {
		// TODO Auto-generated method stub
		return MaxFatigue;
	}

	@Override
	public void setMaxFatigue(float value) {
		// TODO Auto-generated method stub
		MaxFatigue = value;
	}

	@Override
	public float getMinFatigue() {
		// TODO Auto-generated method stub
		return MinFatigue;
	}

	@Override
	public void setMinFatigue(float value) {
		// TODO Auto-generated method stub
		MinFatigue = value;
	}

	@Override
	public float getFatigue() {
		// TODO Auto-generated method stub
		return Fatigue;
	}

	@Override
	public void setFatigue(float value) {
		// TODO Auto-generated method stub
		Fatigue = value;
	}

}
