package entites;

import interfaces.Drink;

public abstract class Drinks implements Drink {
	
	protected float maxThirst = 100;
	protected float minThirst = 100;
	protected float thirst = 100;

	public Drinks() {
		// TODO Auto-generated constructor stub
	}
	
	public Drinks(float maxThirst,float minThrist) {
		// TODO Auto-generated constructor stub
		this.maxThirst = maxThirst;
		this.minThirst = minThrist;
	}

	@Override
	public void drink(float quench) {
		// TODO Auto-generated method stub
		if ((thirst += quench) > maxThirst) thirst = maxThirst;
		else thirst += quench;
	}

	@Override
	public void dehydrate(float dehydrate) {
		// TODO Auto-generated method stub
		if ((thirst -= dehydrate) < 0) thirst = 0;
		else thirst -= dehydrate;
	}

	@Override
	public float getMaxThirst() {
		// TODO Auto-generated method stub
		return maxThirst;
	}

	@Override
	public void setMaxThirst(float value) {
		// TODO Auto-generated method stub
		maxThirst = value;
	}

	@Override
	public float getMinThirst() {
		// TODO Auto-generated method stub
		return minThirst;
	}

	@Override
	public void setMinThirst(float value) {
		// TODO Auto-generated method stub
		minThirst = value;
	}

	@Override
	public float getThirst() {
		// TODO Auto-generated method stub
		return thirst;
	}

	@Override
	public void setThirst(float value) {
		// TODO Auto-generated method stub
		thirst = value;
	}

}
