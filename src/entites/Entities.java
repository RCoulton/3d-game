package entites;

import objects.Model;

import org.lwjgl.util.vector.Vector3f;
import org.newdawn.slick.opengl.Texture;

import interfaces.Entity;

public abstract class Entities implements Entity {
	
	protected Model Mesh;
	protected Texture Texture;
	protected Vector3f Scale;
	protected float Speed;
	protected String Name;

	public Entities() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public String getName() {
		// TODO Auto-generated method stub
		return Name;
	}

	@Override
	public void setName(String name) {
		// TODO Auto-generated method stub
		Name = name;
	}

	@Override
	public void setTexture(Texture tex) {
		// TODO Auto-generated method stub
		Texture = tex;
	}

	@Override
	public void setModel(Model mod) {
		// TODO Auto-generated method stub
		Mesh = mod;
	}

	@Override
	public Vector3f getScale() {
		// TODO Auto-generated method stub
		return Scale;
	}

	@Override
	public void setScale(Vector3f scale) {
		// TODO Auto-generated method stub
		Scale = scale;
	}

}
