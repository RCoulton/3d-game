package camera;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.util.glu.GLU;


import static java.lang.Math.*;
import static org.lwjgl.opengl.ARBDepthClamp.GL_DEPTH_CLAMP;
import static org.lwjgl.opengl.GL11.*;


public final class EulerCamera implements Camera {
	
	private float x = 0;
	private float y = 0;
	private float z = 0;
	private float pitch = 0;
	private float yaw = 0;
	private float roll = 0;
	private float fov = 90;
	private float aspectRatio = 1;
	private final float zNear;
	private final float zFar;
	
	private EulerCamera(Builder builder)
	{
		this.x = builder.x;
		this.y = builder.y;
		this.z = builder.z;
		this.pitch = builder.pitch;
		this.yaw = builder.yaw;
		this.roll = builder.roll;
		this.aspectRatio = builder.aspectRatio;
		this.zNear = builder.zNear;
		this.zFar = builder.zFar;
		this.fov = builder.fov;
	}
	
	public EulerCamera()
	{
		this.zNear = 0.3f;
		this.zFar = 100;
	}
	
	public EulerCamera(float aspectRatio)
	{
		if (aspectRatio <= 0) {
            throw new IllegalArgumentException("aspectRatio " + aspectRatio + " was 0 or was smaller than 0");
        }
        this.aspectRatio = aspectRatio;
        this.zNear = 0.3f;
        this.zFar = 100;
	}
	
	public EulerCamera(float aspectRatio, float x, float y, float z) {
        this(aspectRatio);
        this.x = x;
        this.y = y;
        this.z = z;
    }

	public EulerCamera(float aspectRatio, float x, float y, float z, float pitch, float yaw, float roll) {
        this(aspectRatio, x, y, z);
        this.pitch = pitch;
        this.yaw = yaw;
        this.roll = roll;
    }
	
	public EulerCamera(float aspectRatio, float x, float y, float z, float pitch, float yaw, float roll, float zNear, float zFar) 
	{
		if (aspectRatio <= 0) 
		{
			throw new IllegalArgumentException("aspectRatio " + aspectRatio + " was 0 or was smaller than 0");
		}
		if (zNear <= 0)
		{
			throw new IllegalArgumentException("zNear " + zNear + " was 0 or was smaller than 0");
		}
		if (zFar <= zNear) 
		{
			throw new IllegalArgumentException("zFar " + zFar + " was smaller or the same as zNear " + zNear);
		}
		
		this.aspectRatio = aspectRatio;
		this.x = x;
		this.y = y;
		this.z = z;
		this.pitch = pitch;
		this.yaw = yaw;
		this.roll = roll;
		this.zNear = zNear;
		this.zFar = zFar;
	}

	
	
	@Override
	public void processMouse() 
	{
		final float MAX_LOOK_UP = 90;
		final float MAX_LOOK_DOWN = -90;
		float mouseDX = Mouse.getDX() * 0.16f;
		float mouseDY = Mouse.getDY() * 0.16f;
		
		if(yaw + mouseDX >= 360)
		{
			yaw = yaw + mouseDX - 360;
		}
		else if(yaw + mouseDX < 0)
		{
			yaw = 360-yaw+ mouseDX;
		}
		else
		{
			yaw += mouseDX;
		}
		
		if(pitch-mouseDY >= MAX_LOOK_DOWN && pitch - mouseDY <= MAX_LOOK_UP)
		{
			pitch += -mouseDY;
		}
		else if(pitch - mouseDY < MAX_LOOK_DOWN)
		{
			pitch = MAX_LOOK_DOWN;
		}
		else if(pitch - mouseDY >MAX_LOOK_UP)
		{
			pitch = MAX_LOOK_UP;
		}

	}

	@Override
	public void processMouse(float mouseSpeed) 
	{
		final float MAX_LOOK_UP = 90;
		final float MAX_LOOK_DOWN = -90;
		float mouseDX = Mouse.getDX() * mouseSpeed * 0.16f;
		float mouseDY = Mouse.getDY() * mouseSpeed * 0.16f;
		
		if(yaw + mouseDX >= 360)
		{
			yaw = yaw + mouseDX - 360;
		}
		else if(yaw + mouseDX < 0)
		{
			yaw = 360-yaw+ mouseDX;
		}
		else
		{
			yaw += mouseDX;
		}
		
		if(pitch-mouseDY >= MAX_LOOK_DOWN && pitch - mouseDY <= MAX_LOOK_UP)
		{
			pitch += -mouseDY;
		}
		else if(pitch - mouseDY < MAX_LOOK_DOWN)
		{
			pitch = MAX_LOOK_DOWN;
		}
		else if(pitch - mouseDY >MAX_LOOK_UP)
		{
			pitch = MAX_LOOK_UP;
		}
	}

	@Override
	public void processMouse(float mouseSpeed, float maxLookUp, float maxLookDown) 
	{
		float mouseDX = Mouse.getDX() * mouseSpeed * 0.16f;
		float mouseDY = Mouse.getDY() * mouseSpeed * 0.16f;
		
		if(yaw + mouseDX >= 360)
		{
			yaw = yaw + mouseDX - 360;
		}
		else if(yaw + mouseDX < 0)
		{
			yaw = 360-yaw+ mouseDX;
		}
		else
		{
			yaw += mouseDX;
		}
		
		if(pitch-mouseDY >= maxLookDown && pitch - mouseDY <= maxLookUp)
		{
			pitch += -mouseDY;
		}
		else if(pitch - mouseDY < maxLookDown)
		{
			pitch = maxLookDown;
		}
		else if(pitch - mouseDY >maxLookUp)
		{
			pitch = maxLookUp;
		}
	}

	@Override
	public void processKeyboard(float delta) 
	{
		if(delta<=0)
		{
			delta = 0;
		}
		
		boolean keyUp = Keyboard.isKeyDown(Keyboard.KEY_W);
		boolean keyDown = Keyboard.isKeyDown(Keyboard.KEY_S);
		boolean keyLeft = Keyboard.isKeyDown(Keyboard.KEY_A);
		boolean keyRight = Keyboard.isKeyDown(Keyboard.KEY_D);
		//boolean flyUp = Keyboard.isKeyDown(Keyboard.KEY_SPACE);
		//boolean flyDown = Keyboard.isKeyDown(Keyboard.KEY_LSHIFT);
		
		if(keyUp && keyRight && !keyLeft && !keyDown)
		{
			moveFromLook(delta*0.003f, 0, -delta*0.003f);
		}
		if(keyUp && keyLeft && !keyRight && !keyDown)
		{
			moveFromLook(-delta*0.003f, 0, -delta*0.003f);
		}
		if(keyUp && !keyDown && !keyLeft && !keyRight)
		{
			moveFromLook(0,0,-delta*0.003f);
		}
		
		if(keyDown && keyRight && !keyLeft && !keyUp)
		{
			moveFromLook(-delta*0.003f, 0, delta*0.003f);
		}
		if(keyDown && keyLeft && !keyRight && !keyUp)
		{
			moveFromLook(delta*0.003f, 0, delta*0.003f);
		}
		if(keyDown && !keyUp && !keyLeft && !keyRight)
		{
			moveFromLook(0,0,delta*0.003f);
		}
		
		if(keyLeft && !keyRight && !keyUp && !keyDown)
		{
			moveFromLook(-delta*0.003f, 0, 0);
		}
		if(keyRight && !keyLeft && !keyUp && !keyDown)
		{
			moveFromLook(delta*0.003f, 0, 0);
		}
		/*
		if(flyUp && !flyDown)
		{
			y+= delta*0.003f;
		}
		
		if(flyDown && !flyUp)
		{
			y-= delta*0.003f;
		}
		*/
	}

	@Override
	public void processKeyboard(float delta, float speed) {
		if(delta<=0)
		{
			throw new IllegalArgumentException("delta " + delta + " is 0 or is smaller than 0");
		}
		
		boolean keyUp = Keyboard.isKeyDown(Keyboard.KEY_W);
		boolean keyDown = Keyboard.isKeyDown(Keyboard.KEY_S);
		boolean keyLeft = Keyboard.isKeyDown(Keyboard.KEY_A);
		boolean keyRight = Keyboard.isKeyDown(Keyboard.KEY_D);
		//boolean flyUp = Keyboard.isKeyDown(Keyboard.KEY_SPACE);
		//boolean flyDown = Keyboard.isKeyDown(Keyboard.KEY_LSHIFT);
		
		if(keyUp && keyRight && !keyLeft && !keyDown)
		{
			moveFromLook(speed * delta*0.003f, 0, -speed * delta*0.003f);
		}
		if(keyUp && keyLeft && !keyRight && !keyDown)
		{
			moveFromLook(-speed * delta*0.003f, 0, -speed * delta*0.003f);
		}
		if(keyUp && !keyDown && !keyLeft && !keyRight)
		{
			moveFromLook(0,0,-speed * delta*0.003f);
		}
		
		if(keyDown && keyRight && !keyLeft && !keyUp)
		{
			moveFromLook(-speed * delta*0.003f, 0, speed * delta*0.003f);
		}
		if(keyDown && keyLeft && !keyRight && !keyUp)
		{
			moveFromLook(speed * delta*0.003f, 0, speed * delta*0.003f);
		}
		if(keyDown && !keyUp && !keyLeft && !keyRight)
		{
			moveFromLook(0,0,speed * delta*0.003f);
		}
		
		if(keyLeft && !keyRight && !keyUp && !keyDown)
		{
			moveFromLook(-speed * delta*0.003f, 0, 0);
		}
		if(keyRight && !keyLeft && !keyUp && !keyDown)
		{
			moveFromLook(speed * delta*0.003f, 0, 0);
		}
		/*
		if(flyUp && !flyDown)
		{
			y+= speed * delta*0.003f;
		}
		
		if(flyDown && !flyUp)
		{
			y-= speed * delta*0.003f;
		}
		*/

	}

	@Override
	public void processKeyboard(float delta, float speedX, float speedY,
			float speedZ) {
		if(delta<=0)
		{
			delta = 0;
		}
		
		boolean keyUp = Keyboard.isKeyDown(Keyboard.KEY_W);
		boolean keyDown = Keyboard.isKeyDown(Keyboard.KEY_S);
		boolean keyLeft = Keyboard.isKeyDown(Keyboard.KEY_A);
		boolean keyRight = Keyboard.isKeyDown(Keyboard.KEY_D);
		boolean runKey = Keyboard.isKeyDown(Keyboard.KEY_LSHIFT);
		//boolean flyUp = Keyboard.isKeyDown(Keyboard.KEY_SPACE);
		//boolean flyDown = Keyboard.isKeyDown(Keyboard.KEY_LSHIFT);
		
		if(keyUp && keyRight && !keyLeft && !keyDown)
		{
			if(runKey)
			{
				moveFromLook((speedX+3) * delta*0.003f, 0, -(speedZ+3) * delta*0.003f);
			}
			else
			{
				moveFromLook(speedX * delta*0.003f, 0, -speedZ * delta*0.003f);
			}
			//move(speedX * delta*0.003f, 0, -speedZ * delta*0.003f);
		}
		if(keyUp && keyLeft && !keyRight && !keyDown)
		{
			if(runKey)
			{
				moveFromLook(-(speedX+3) * delta*0.003f, 0, -(speedZ+3) * delta*0.003f);
			}
			else
			{
				moveFromLook(-speedX * delta*0.003f, 0, -speedZ * delta*0.003f);
			}
			//move(-speedX * delta*0.003f, 0, -speedZ * delta*0.003f);
		}
		if(keyUp && !keyDown && !keyLeft && !keyRight)
		{
			if(runKey)
			{
				moveFromLook(0,0,-(speedZ +3) * delta * 0.003f);
			}
			else
			{
				moveFromLook(0,0,-speedZ * delta*0.003f);
			}
			
			//move(0,0,-speedZ * delta*0.003f);
		}
		
		if(keyDown && keyRight && !keyLeft && !keyUp)
		{
			moveFromLook(speedX * delta*0.003f, 0, speedZ * delta*0.003f);
			//move(speedX * delta*0.003f, 0, speedZ * delta*0.003f);
		}
		if(keyDown && keyLeft && !keyRight && !keyUp)
		{
			moveFromLook(-speedX * delta*0.003f, 0, speedZ * delta*0.003f);
			//move(-speedX * delta*0.003f, 0, speedZ * delta*0.003f);
		}
		if(keyDown && !keyUp && !keyLeft && !keyRight)
		{
			moveFromLook(0,0,speedZ * delta*0.003f);
			//move(0,0,speedZ * delta*0.003f);
		}
		
		if(keyLeft && !keyRight && !keyUp && !keyDown)
		{
			moveFromLook(-speedX * delta*0.003f, 0, 0);
			//move(-speedX * delta*0.003f, 0, 0);
		}
		if(keyRight && !keyLeft && !keyUp && !keyDown)
		{
			moveFromLook(speedX * delta*0.003f, 0, 0);
			//move(speedX * delta*0.003f, 0, 0);
		}
		
		/*
		if(flyUp && !flyDown)
		{
			y+= speedY * delta*0.003f;
		}
		
		if(flyDown && !flyUp)
		{
			y-= speedY * delta*0.003f;
		}
		*/

	}

	@Override
	public void moveFromLook(float dx, float dy, float dz)
	{
		this.z += dx * (float)cos(toRadians(yaw-90)) + dz * cos(toRadians(yaw));
		this.x -= dx * (float)sin(toRadians(yaw - 90)) + dz * sin(toRadians(yaw));
		//this.y += dy * (float)sin(toRadians(pitch -90)) + dz * sin(toRadians(pitch));
	}
	
	public void move(float dx, float dy, float dz)
	{
		this.z += dz;
		this.x += dx;
	}
	

	@Override
	public void setPosition(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;

	}

	@Override
	public void applyOrthographicMatrix() 
	{
		glPushAttrib(GL_TRANSFORM_BIT);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(-aspectRatio, aspectRatio, -1, 1, 0, zFar);
		glPopAttrib();
	}

	@Override
	public void applyOptimalStates() 
	{
		if(GLContext.getCapabilities().GL_ARB_depth_clamp)
		{
			glEnable(GL_DEPTH_CLAMP);
		}
	}

	@Override
	public void applyPerspectiveMatrix() 
	{
		glPushAttrib(GL_TRANSFORM_BIT);
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		GLU.gluPerspective(fov, aspectRatio, zNear, zFar);
		glPopAttrib();
	}

	@Override
	public void applyTranslations() 
	{
		glPushAttrib(GL_TRANSFORM_BIT);
		glMatrixMode(GL_MODELVIEW);
		glRotatef(pitch, 1, 0, 0);
		glRotatef(yaw, 0, 1, 0);
		glRotatef(roll, 0, 0, 1);
		glTranslatef(-x, -y, -z);
		glPopAttrib();

	}

	@Override
	public void setRotation(float pitch, float yaw, float roll) 
	{
		this.pitch = pitch;
		this.yaw = yaw;
		this.roll = roll;

	}

	@Override
	public float x() 
	{
		return x;
	}

	@Override
	public float y() 
	{
		return y;
	}

	@Override
	public float z() 
	{
		return z;
	}

	@Override
	public float pitch()
	{
		return pitch;
	}

	@Override
	public float yaw()
	{
		return yaw;
	}

	@Override
	public float roll()
	{
		return roll;
	}

	@Override
	public float fieldOfView() 
	{
		return fov;
	}

	@Override
	public void setFieldOfView(float fov) 
	{
		this.fov = fov;
	}

	@Override
	public void setAspectRatio(float aspectRatio) 
	{
		if (aspectRatio <= 0) {
            throw new IllegalArgumentException("aspectRatio " + aspectRatio + " is 0 or less");
        }
        this.aspectRatio = aspectRatio;
	}

	@Override
	public float aspectRatio()
	{
		return aspectRatio;
	}

	@Override
	public float nearClippingPane() 
	{
		return zNear;
	}

	@Override
	public float farClippingPane() 
	{
		return zFar;
	}
	
	@Override
    public String toString() {
        return "EulerCamera [x=" + x + ", y=" + y + ", z=" + z + ", pitch=" + pitch + ", yaw=" + yaw + ", " +
                "roll=" + roll + ", fov=" + fov + ", aspectRatio=" + aspectRatio + ", zNear=" + zNear + ", " +
                "zFar=" + zFar + "]";
    }

	
	public static class Builder
	{
		private float aspectRatio =1;
		private float x = 0;
		private float y = 0;
		private float z = 0;
		private float pitch = 0;
		private float yaw = 0;
		private float roll = 0;
		private float fov = 90;
		private float zNear = 0.3f;
		private float zFar = 100;
		
		public Builder()
		{}
		
		public Builder setAspectRatio(float aspectRatio)
		{
			if(aspectRatio <= 0)
			{
				throw new IllegalArgumentException("aspectRatio "+ aspectRatio + " was 0 or was smaller than 0");
			}
			this.aspectRatio = aspectRatio;
			return this;
		}
		
		public Builder setNearClippingPane(float nearClippingPlane)
		{
			if(nearClippingPlane <= 0)
			{
				throw new IllegalArgumentException("nearClippingPlane "+ nearClippingPlane + " was 0 or was smaller than 0");
			}
			this.zNear = nearClippingPlane;
			return this;
		}
		
		public Builder setFarClippingPane(float farClippingPlane)
		{
			if(farClippingPlane <= 0)
			{
				throw new IllegalArgumentException("nearClippingPlane "+ farClippingPlane + " was 0 or was smaller than 0");
			}
			this.zNear = farClippingPlane;
			return this;
		}
		
		public Builder setFOV(float fov)
		{
			this.fov = fov;
			return this;
		}
		
		public Builder setPosition(float x, float y, float z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			return this;
		}
		
		public Builder setRotation(float pitch, float yaw, float roll)
		{
			this.pitch = pitch;
			this.yaw = yaw;
			this.roll = roll;
			return this;
		}
		
		public EulerCamera build()
		{
			if (zFar <= zNear) {
                throw new IllegalArgumentException("farClippingPane " + zFar + " is the same or less than " +
                        "nearClippingPane " + zNear);
            }
            return new EulerCamera(this);

		}
	}

}
