package loaders;

import java.io.IOException;

import utility.Generators;

public class FileLoader {

	public FileLoader() {
		// TODO Auto-generated constructor stub
	}
	
	public static void LoadFiles() throws IOException
	{
		// Just throw all of your file loading that only needs to get done once here, and then it'll get called when the game loads or something.
		
		Generators.InitializeHumanFirstFile();
		Generators.InitializeHumanLastFile();
		Generators.InitializeHumanTitleFile();
		Generators.InitializeAdjectiveFile();
		Generators.InitializeNounFile();
		Generators.InitializeVerbFile();
	}

}
