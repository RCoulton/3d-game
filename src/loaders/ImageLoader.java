package loaders;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;

public class ImageLoader {

	public ImageLoader() {
		// TODO Auto-generated constructor stub
	}
	
	public static Texture loadTexture(String key, String type)
	{	
            switch (type.toUpperCase()) 
		{
			case "PNG":
				try 
				{
					return TextureLoader.getTexture("PNG", new FileInputStream(new File("res/textures/" + key + ".png")));
				} 
				catch (FileNotFoundException e) {e.printStackTrace();}
				catch (IOException e) {e.printStackTrace();}
				break;

			default:
				break;
		}
		return null;
                
	}

}
