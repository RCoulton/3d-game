package loaders;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.ArrayList;

import objects.Model;
import loaders.OBJLoader;

public class AnimationLoader {

	public static List<Model> loadAnimation (String file, int frames)
	{
		List<Model> anim = new ArrayList<Model>();
		String tempFileLocation = "";
				
		for (int i = 1; i <= frames; i++)
		{
			if (i < 10)
			{
				tempFileLocation = file + "_00000" + i + ".obj";
			}
			else
			{
				tempFileLocation = file + "_0000" + i+ ".obj";
			}
			try 
			{
				anim.add(OBJLoader.loadModel(new File(tempFileLocation)));
			} 
			catch (IOException e) 
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		if (!anim.isEmpty())
		{
			return anim;
		}
		
		return null;
	}
	
}
