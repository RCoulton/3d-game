package utility;

import org.lwjgl.Sys;

public class Time {

	private static long lastFrame;
	
	public static long getTime()
	{
		return (Sys.getTime()* 1000)/Sys.getTimerResolution();
	}
	
	public static int getDelta()
	{
		long current = getTime();
		int delta = (int)(current - lastFrame);
		lastFrame = getTime();
		return delta;
	}
	
	public static void getLastFrame()
	{
		lastFrame = getTime();
	}
	
	
}
