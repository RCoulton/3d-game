package utility;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Generators {
	
	// TODO: Add entries to the various files used by the generators.
	
	// These have been made during the *broken* period, and thus only work in theory.  Will need debugging once the project actually works.
	// One can generate a random noun or adjective, which can be used elsewhere to form things like, 'the adjective noun', 'nounnoun', 'adjectivenoun', etc...
	// The nouns/adjectives are pretty generic, maybe we could do a symbol system such as that of Dwarf Fortress (every word has a 'set' it belongs to, such as flowery or evil, which describes the word).
	
	static ArrayList<String> HumanFirstPossibilities = new ArrayList<String>();
	static ArrayList<String> HumanLastPossibilities = new ArrayList<String>();
	static ArrayList<String> HumanSyllablePossibilities = new ArrayList<String>();
	static ArrayList<String> HumanTitlePossibilities = new ArrayList<String>();
	static ArrayList<String> NounPossibilities = new ArrayList<String>();
	static ArrayList<String> AdjectivePossibilities = new ArrayList<String>();
	static ArrayList<String> VerbPossibilities = new ArrayList<String>();
	
	// Void Initialization Methods
	
	public static void InitializeHumanFirstFile() throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader("res/generators/human_first.txt"));
		String line;
		while ((line = br.readLine()) != null) {
		   HumanFirstPossibilities.add(line);
		}
		br.close();
		
		BufferedReader bbr = new BufferedReader(new FileReader("res/generators/human_syllable.txt"));
		String lline;
		while ((lline = bbr.readLine()) != null) {
		   HumanSyllablePossibilities.add(lline);
		}
		bbr.close();
	}
	
	public static void InitializeHumanLastFile() throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader("res/generators/human_last.txt"));
		String line;
		while ((line = br.readLine()) != null) {
		   HumanLastPossibilities.add(line);
		}
		br.close();
	}
	
	public static void InitializeHumanTitleFile() throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader("res/generators/human_titles.txt"));
		String line;
		while ((line = br.readLine()) != null) {
		   HumanTitlePossibilities.add(line);
		}
		br.close();
	}
	
	public static void InitializeNounFile() throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader("res/generators/noun.txt"));
		String line;
		while ((line = br.readLine()) != null) {
		   NounPossibilities.add(line);
		}
		br.close();
	}
	
	public static void InitializeAdjectiveFile() throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader("res/generators/adjective.txt"));
		String line;
		while ((line = br.readLine()) != null) {
		   AdjectivePossibilities.add(line);
		}
		br.close();
	}
	
	public static void InitializeVerbFile() throws IOException
	{
		BufferedReader br = new BufferedReader(new FileReader("res/generators/verb.txt"));
		String line;
		while ((line = br.readLine()) != null) {
		   VerbPossibilities.add(line);
		}
		br.close();
	}
	
	// String *useable* Methods
	
	public static String randHumanFirst()
	{
		String result;
		result = HumanFirstPossibilities.get((int) (Math.random() * HumanFirstPossibilities.size()));
		return result;
	}
	
	public static String randHumanFirst(int num) // Overload for using random syllables
	{
		String result = "";
		for (num = num; num > 0; num--)
		{
			result += HumanSyllablePossibilities.get((int) (Math.random() * HumanSyllablePossibilities.size()));
		}
		return result;
	}
	
	public static String randHumanLast()
	{
		String result;
		result = HumanLastPossibilities.get((int) (Math.random() * HumanLastPossibilities.size()));
		return result;
	}
	
	public static String randHumanTitle()
	{
		String result;
		result = HumanTitlePossibilities.get((int) (Math.random() * HumanTitlePossibilities.size()));
		return result;
	}
	
	public static String randNoun(boolean yn) // The bool tells if you want plural
	{
		StringBuilder step1 = new StringBuilder();
		String result = null;
		
		step1.append(NounPossibilities.get((int) (Math.random() * NounPossibilities.size())));
		
		if (yn == false)
		{
			step1.delete(step1.indexOf("|"), step1.length());
			result = step1.toString();
		}
		else if (yn == true)
		{
			step1.delete(0, step1.indexOf("|") + 1);
			result = step1.toString();
		}
		
		return result;
	}
	
	public static String randAdjective()
	{
		String result;
		result = AdjectivePossibilities.get((int) (Math.random() * AdjectivePossibilities.size()));
		return result;
	}
	
	public static String randVerb(boolean yn) // The char tells if you want 'ing' at the end, so rather than 'of die' you get 'of dying'
	{
		StringBuilder step1 = new StringBuilder();
		String result = null;
		
		step1.append(VerbPossibilities.get((int) (Math.random() * VerbPossibilities.size())));
		
		if (yn == false)
		{
			step1.delete(step1.indexOf("|"), step1.length());
			result = step1.toString();
		}
		else if (yn == true)
		{
			step1.delete(0, step1.indexOf("|") + 1);
			result = step1.toString();
		}
		
		return result;
	}
}
