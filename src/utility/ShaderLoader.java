package utility;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import static org.lwjgl.opengl.GL11.GL_FALSE;
import static org.lwjgl.opengl.GL20.*;

public class ShaderLoader 
{
	public static int loadShaderPair(String vertexShaderLocation, String fragmentShaderLocation)
	{
		int shaderProgram = glCreateProgram();
		int vertexShader = glCreateShader(GL_VERTEX_SHADER);
		int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
		StringBuilder vertexShaderSource = new StringBuilder();
		StringBuilder fragmentShaderSource = new StringBuilder();
		
		BufferedReader vertexShaderFileReader = null;		
		try
		{
			vertexShaderFileReader = new BufferedReader(new FileReader(vertexShaderLocation));
			String line;
			while((line = vertexShaderFileReader.readLine())!= null)
			{
				vertexShaderSource.append(line).append('\n');
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return -1;
		}
		finally
		{
			if(vertexShaderFileReader != null)
			{
				try {
					vertexShaderFileReader.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		
		BufferedReader fragmentShaderFileReader = null;
		try
		{
			fragmentShaderFileReader = new BufferedReader(new FileReader(fragmentShaderLocation));
			String line;
			while((line = fragmentShaderFileReader.readLine())!=null)
			{
				fragmentShaderSource.append(line).append('\n');
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
			return -1;
		}
		finally
		{
			if(fragmentShaderFileReader != null)
			{
				try {
					fragmentShaderFileReader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
		//Compile Vertex Shader
		glShaderSource(vertexShader, vertexShaderSource);
		glCompileShader(vertexShader);
		if(glGetShaderi(vertexShader, GL_COMPILE_STATUS)== GL_FALSE)
		{
			System.err.println("Vertex shader wasn't able to be compiled correctly. Error log:");
            System.err.println(glGetShaderInfoLog(vertexShader, 1024));
            return -1;
		}
		
		//Compile Fragment Shader
		glShaderSource(fragmentShader, fragmentShaderSource);
		glCompileShader(fragmentShader);
		if(glGetShaderi(vertexShader, GL_COMPILE_STATUS)== GL_FALSE)
		{
			System.err.println("Fragment shader wasn't able to be compiled correctly. Error log:");
            System.err.println(glGetShaderInfoLog(fragmentShader, 1024));
		}
		
		//Linking the shaders
		glAttachShader(shaderProgram, vertexShader);
		glAttachShader(shaderProgram, fragmentShader);
		glLinkProgram(shaderProgram);
		if(glGetProgrami(shaderProgram, GL_LINK_STATUS)==GL_FALSE)
		{
			System.err.println("Shader program wasn't linked correctly.");
            System.err.println(glGetProgramInfoLog(shaderProgram, 1024));
            return -1;
		}
		
		//cleanUp
		glDeleteShader(vertexShader);
		glDeleteShader(fragmentShader);
		return shaderProgram;
	}
	
//	public static int loadShaderPairMulti(String vertexShaderLocation, String fragmentShaderLocation)
//	{
//		int shader=0;
//	    int vertShader=0;
//	    int fragShader=0;
//		
//	    shader=ARBShaderObjects.glCreateProgramObjectARB();
//	    if (shader!=0)
//	    {
//	    	vertShader=createVertShader("res/shaders/file.vert");
//	    	fragShader=createFragShader("res/shaders/file.frag");
//	    }
//      
//	    if(vertShader!=0 && fragShader!=0)
//	    {
//	    	ARBShaderObjects.glAttachObjectARB(shader, vertShader);
//	    	ARBShaderObjects.glAttachObjectARB(shader, fragShader);
//	    	ARBShaderObjects.glLinkProgramARB(shader);
//	    	ARBShaderObjects.glValidateProgramARB(shader);      	
//	    }
//				
//	    //cleanUp
//		glDeleteShader(vertShader);
//		glDeleteShader(fragShader);
//		return shader;
//	}
//	
//  private static int createVertShader (String filename)
//  {
//  	int vertShader = ARBShaderObjects.glCreateShaderObjectARB(ARBVertexShader.GL_VERTEX_SHADER_ARB);
//  	if(vertShader==0)return 0;
//  	String vertexCode="";
//  	String line;
//  	try
//  	{
//  		BufferedReader reader= new BufferedReader (new FileReader(filename));
//  		while((line=reader.readLine())!=null)
//  		{
//  			vertexCode+=line + "\n";
//  		}
//  		reader.close();
//  	}
//  	catch (Exception e)
//  	{
//  		System.out.println("Fail reading vertex shading code");
//  		return 0;
//  	}
//  	ARBShaderObjects.glShaderSourceARB(vertShader, vertexCode);
//  	ARBShaderObjects.glCompileShaderARB(vertShader);
//  	if(!printLogInfo(vertShader))
//  	{
//  		vertShader=0;
//  	}
//  	return vertShader;
//  }
//  
//  private static int createFragShader (String filename)
//  {
//  	int fragShader = ARBShaderObjects.glCreateShaderObjectARB(ARBFragmentShader.GL_FRAGMENT_SHADER_ARB);
//  	if(fragShader==0)return 0;
//  	String fragCode="";
//  	String line;
//  	try
//  	{
//  		BufferedReader reader= new BufferedReader (new FileReader(filename));
//  		while((line=reader.readLine())!=null)
//  		{
//  			fragCode+=line + "\n";
//  		}
//  		reader.close();
//  	}
//  	catch (Exception e)
//  	{
//  		System.out.println("Fail reading fragment shading code");
//  		return 0;
//  	}
//  	ARBShaderObjects.glShaderSourceARB(fragShader, fragCode);
//  	ARBShaderObjects.glCompileShaderARB(fragShader);
//  	if(!printLogInfo(fragShader))
//  	{
//  		fragShader=0;
//  	}
//  	return fragShader;
//  }
//  
//  private static boolean printLogInfo(int obj)
//  {
//  	IntBuffer iVal = BufferUtils.createIntBuffer(1);
//  	ARBShaderObjects.glGetObjectParameterARB(obj,ARBShaderObjects.GL_OBJECT_INFO_LOG_LENGTH_ARB, iVal);
//  	int length = iVal.get();
//  	if(length > 1)
//  	{
//  		ByteBuffer infoLog = BufferUtils.createByteBuffer(length);
//  		iVal.flip();
//  		ARBShaderObjects.glGetInfoLogARB(obj, iVal, infoLog);
//  		byte[] infoBytes = new byte[length];
//  		infoLog.get(infoBytes);
//  		String out = new String(infoBytes);
//  		System.out.println("Info log:\n"+out);
//  	}
//  	else return false;
//  	return true;    		
//  }
	
}
