package utility;

import org.lwjgl.BufferUtils;
import org.lwjgl.util.vector.Matrix4f;
import org.lwjgl.util.vector.Vector3f;


import java.nio.ByteBuffer;
import java.nio.FloatBuffer;


public class BufferTools 
{
    public static FloatBuffer asFlippedFloatBuffer(Matrix4f matrix4f) 
    {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        matrix4f.store(buffer);
        buffer.flip();
        return buffer;
    }
    
    public static FloatBuffer asFlippedFloatBuffer(float... values) 
    {
        FloatBuffer buffer = BufferUtils.createFloatBuffer(values.length);
        buffer.put(values);
        buffer.flip();
        return buffer;
    }
    
    public static FloatBuffer reserveData(int amountOfElements) 
    {
        return BufferUtils.createFloatBuffer(amountOfElements);
    }
    
    public static ByteBuffer asByteBuffer(byte... values) {
        ByteBuffer buffer = BufferUtils.createByteBuffer(values.length);
        buffer.put(values);
        return buffer;
    }
    
    public static float[] asFloats(Vector3f v) {
        return new float[]{v.x, v.y, v.z};
    }




}
