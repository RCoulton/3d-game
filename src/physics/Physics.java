package physics;

public class Physics 
{
	private final static float gAccel = 3.8f;
	
	public Physics ()
	{
		
	}
	public static float gravity(float velocity, float fps)
	{
		return velocity - (gAccel/fps);
	}
}
