uniform sampler2D sampler01;
uniform sampler2D sampler02;
varying vec4 vertColor;
void main()
{
	vec3 theColor=vec3(texture2D(sampler01, (gl_TexCoord[0].st)));
	vec3 theColor2=vec3(texture2D(sampler02, (gl_TexCoord[0].st)));
	if(theColor2.r > 0.5)
	{
		theColor2=vec3(1.0,1.0,1.0);
	}
	gl_FragColor = vec4(theColor2*theColor,1.0);
}